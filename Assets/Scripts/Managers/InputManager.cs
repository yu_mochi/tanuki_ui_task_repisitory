﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using nn.hid;

public class InputManager : SingletonMonoBehaviour<InputManager>, IDontDestroy
{
    private int mEnabledInputCount;
    private NpadState mCurrentState = new NpadState();
    private NpadState mOldState;
    private NpadId mNpadId = NpadId.Invalid;

    private Dictionary<KeyCode, NpadButton> mDebugKeyConfig = new Dictionary<KeyCode, NpadButton>();
    private AnalogStickState mOldAnalogStickLState;
    private AnalogStickState mOldAnalogStickRState;
    private long mOldButtonState;
    public bool IsRockedExceptStickR { get; set; }

    private static readonly KeyCode[] KEY_CODES = System.Enum.GetValues(typeof(KeyCode)) as KeyCode[];

    /// <summary>
    /// ボタンが押された時に呼ばれるコールバック
    /// </summary>
    public event System.Action<NpadButton> InputButtonDownCallback;

    /// <summary>
    /// ボタンが押されている時に呼ばれるコールバック
    /// </summary>
    public event System.Action<NpadButton> InputButtonCallback;

    /// <summary>
    /// ボタンが離された際に呼ばれるコールバック
    /// </summary>
    public event System.Action<NpadButton> InputButtonUpCallback;

    /// <summary>
    /// 左スティックに入力がある時に呼ばれるコールバック
    /// </summary>
    public event System.Action<AnalogStickState> InputAnalogStickLCallback;

    /// <summary>
    /// 左スティックが離された際に呼ばれるコールバック
    /// </summary>
    public event System.Action InputAnalogStickLReleaseCallback;

    /// <summary>
    /// 右スティックに入力がある時に呼ばれるコールバック
    /// </summary>
    public event System.Action<AnalogStickState> InputAnalogStickRCallback;

    /// <summary>
    /// 右スティックが離された際に呼ばれるコールバック
    /// </summary>
    public event System.Action InputAnalogStickRReleaseCallback;

    #region AccessMethod
    /// <summary>
    /// 全ての入力イベントを破棄
    /// </summary>
    public void Dispose()
    {
        InputButtonDownCallback = null;
        InputButtonCallback = null;
        InputButtonUpCallback = null;
        InputAnalogStickLCallback = null;
        InputAnalogStickLReleaseCallback = null;
        InputAnalogStickRCallback = null;
        InputAnalogStickRReleaseCallback = null;
    }

    /// <summary>
    /// 入力許可
    /// </summary>
    public void EnableInput()
    {
        --mEnabledInputCount;
        if (mEnabledInputCount < 0) mEnabledInputCount = 0;
    }

    /// <summary>
    /// 入力禁止
    /// </summary>
    public void DisableInput()
    {
        ++mEnabledInputCount;
    }

    /// <summary>
    /// 入力可能かどうか
    /// </summary>
    /// <returns>[true:入力可能][false:入力不可]</returns>
    public bool CanInput()
    {
        return (mEnabledInputCount == 0);
    }
    #endregion

    #region UnityMethod
    private void Start()
    {
        Npad.Initialize();
        Npad.SetSupportedStyleSet(NpadStyle.FullKey | NpadStyle.Handheld | NpadStyle.JoyDual);
        Npad.SetSupportedIdType(new NpadId[] { NpadId.Handheld, NpadId.No1 });

        mDebugKeyConfig.Add(KeyCode.W, NpadButton.Up);
        mDebugKeyConfig.Add(KeyCode.S, NpadButton.Down);
        mDebugKeyConfig.Add(KeyCode.A, NpadButton.Left);
        mDebugKeyConfig.Add(KeyCode.D, NpadButton.Right);

        mDebugKeyConfig.Add(KeyCode.O, NpadButton.StickL);
        mDebugKeyConfig.Add(KeyCode.P, NpadButton.StickR);

        mDebugKeyConfig.Add(KeyCode.UpArrow, NpadButton.StickLUp);
        mDebugKeyConfig.Add(KeyCode.DownArrow, NpadButton.StickLDown);
        mDebugKeyConfig.Add(KeyCode.LeftArrow, NpadButton.StickLLeft);
        mDebugKeyConfig.Add(KeyCode.RightArrow, NpadButton.StickLRight);

        mDebugKeyConfig.Add(KeyCode.I, NpadButton.StickRUp);
        mDebugKeyConfig.Add(KeyCode.J, NpadButton.StickRDown);
        mDebugKeyConfig.Add(KeyCode.K, NpadButton.StickRLeft);
        mDebugKeyConfig.Add(KeyCode.L, NpadButton.StickRRight);

        mDebugKeyConfig.Add(KeyCode.Alpha1, NpadButton.A);
        mDebugKeyConfig.Add(KeyCode.Alpha2, NpadButton.B);
        mDebugKeyConfig.Add(KeyCode.Alpha3, NpadButton.X);
        mDebugKeyConfig.Add(KeyCode.Alpha4, NpadButton.Y);
        mDebugKeyConfig.Add(KeyCode.Alpha5, NpadButton.L);
        mDebugKeyConfig.Add(KeyCode.Alpha6, NpadButton.R);
        mDebugKeyConfig.Add(KeyCode.Alpha7, NpadButton.ZL);
        mDebugKeyConfig.Add(KeyCode.Alpha8, NpadButton.ZR);
        mDebugKeyConfig.Add(KeyCode.Alpha9, NpadButton.Minus);
        mDebugKeyConfig.Add(KeyCode.Alpha0, NpadButton.Plus);
    }

    private void Update()
    {
        if (!CanInput()) return;

        if (TryUpdatePadState())
        {
            // 入力された時に呼ぶイベントの判定
            NpadButton on_button = mCurrentState.buttons & ~mOldState.buttons;
            if (on_button != 0 && InputButtonDownCallback != null)
            {
                if (!IsRockedExceptStickR) InputButtonDownCallback(on_button);
            }

            // 入力されている間常に呼ぶイベントの判定
            if (mCurrentState.buttons != 0 && InputButtonCallback != null)
            {
                if (!IsRockedExceptStickR) InputButtonCallback(mCurrentState.buttons);
            }
            if ((mCurrentState.analogStickL.x != 0 || mCurrentState.analogStickL.y != 0) && InputAnalogStickLCallback != null)
            {
                if (!IsRockedExceptStickR) InputAnalogStickLCallback(mCurrentState.analogStickL);
            }
            if ((mCurrentState.analogStickR.x != 0 || mCurrentState.analogStickR.y != 0) && InputAnalogStickRCallback != null)
            {
                InputAnalogStickRCallback(mCurrentState.analogStickR);
            }

            // 入力が無くなった時に呼ぶイベントの判定
            NpadButton off_button = ~mCurrentState.buttons & mOldState.buttons;
            if (off_button != 0 && InputButtonUpCallback != null)
            {
                if (!IsRockedExceptStickR) InputButtonUpCallback(off_button);
            }
            if ((mCurrentState.analogStickL.x == 0 && mCurrentState.analogStickL.y == 0) &&
                (mOldState.analogStickL.x != 0 || mOldState.analogStickL.y != 0) && InputAnalogStickLReleaseCallback != null)
            {
                InputAnalogStickLReleaseCallback();
            }
            if ((mCurrentState.analogStickR.x == 0 && mCurrentState.analogStickR.y == 0) &&
                (mOldState.analogStickR.x != 0 || mOldState.analogStickR.y != 0) && InputAnalogStickRReleaseCallback != null)
            {
                InputAnalogStickRReleaseCallback();
            }
            mOldState = mCurrentState;
        }

        // キーボード入力受付用
        NpadButton current_button = GetDebugInputKey();
        if (current_button != 0 && InputButtonCallback != null)
        {
            if (!IsRockedExceptStickR) InputButtonCallback(current_button);
        }

        NpadButton on_debug_button = current_button & ~(NpadButton)mOldButtonState;
        if (on_debug_button != 0 && InputButtonDownCallback != null)
        {
            if (!IsRockedExceptStickR) InputButtonDownCallback(on_debug_button);
        }

        NpadButton off_debug_button = ~current_button & (NpadButton)mOldButtonState;
        if (off_debug_button != 0 && InputButtonUpCallback != null)
        {
            if (!IsRockedExceptStickR) InputButtonUpCallback(off_debug_button);
        }
        mOldButtonState = (long)current_button;

        AnalogStickState current_analog_stick_l_state = GetDebugInputLAxis();
        AnalogStickState current_analog_stick_r_state = GetDebugInputRAxis();
        if (Input.anyKey)
        {
            if ((current_analog_stick_l_state.x != 0 || current_analog_stick_l_state.y != 0) && InputAnalogStickLCallback != null)
            {
                if (!IsRockedExceptStickR) InputAnalogStickLCallback(current_analog_stick_l_state);
            }

            if ((current_analog_stick_r_state.x != 0 || current_analog_stick_r_state.y != 0) && InputAnalogStickRCallback != null)
            {
                InputAnalogStickRCallback(current_analog_stick_r_state);
            }
        }
        else
        {
            if ((current_analog_stick_l_state.x == 0 && current_analog_stick_l_state.y == 0) &&
                (mOldAnalogStickLState.x != 0 || mOldAnalogStickLState.y != 0) && InputAnalogStickLReleaseCallback != null)
            {
                InputAnalogStickLReleaseCallback();
            }

            if ((current_analog_stick_r_state.x == 0 && current_analog_stick_r_state.y == 0) &&
                (mOldAnalogStickRState.x != 0 || mOldAnalogStickRState.y != 0) && InputAnalogStickRReleaseCallback != null)
            {
                InputAnalogStickRReleaseCallback();
            }
        }
        mOldAnalogStickLState = current_analog_stick_l_state;
        mOldAnalogStickRState = current_analog_stick_r_state;
    }
    #endregion

    /// <summary>
    /// 現在の入力状態更新
    /// </summary>
    /// <returns>[true:更新成功][false:更新失敗]</returns>
    private bool TryUpdatePadState()
    {
        NpadStyle handheld_style = Npad.GetStyleSet(NpadId.Handheld);
        NpadState handheld_state = mCurrentState;
        if (handheld_style != NpadStyle.None)
        {
            Npad.GetState(ref handheld_state, NpadId.Handheld, handheld_style);
            if (handheld_state.buttons != NpadButton.None)
            {
                mNpadId = NpadId.Handheld;
                mCurrentState = handheld_state;
                return true;
            }
        }

        NpadStyle no1_style = Npad.GetStyleSet(NpadId.No1);
        NpadState no1_state = mCurrentState;
        if (no1_style != NpadStyle.None)
        {
            Npad.GetState(ref no1_state, NpadId.No1, no1_style);
            if (no1_state.buttons != NpadButton.None)
            {
                mNpadId = NpadId.No1;
                mCurrentState = no1_state;
                return true;
            }
        }

        if ((mNpadId == NpadId.Handheld) && (handheld_style != NpadStyle.None))
        {
            mNpadId = NpadId.Handheld;
            mCurrentState = handheld_state;
        }
        else if ((mNpadId == NpadId.No1) && (no1_style != NpadStyle.None))
        {
            mNpadId = NpadId.No1;
            mCurrentState = no1_state;
        }
        else
        {
            mNpadId = NpadId.Invalid;
            mCurrentState.Clear();
            return false;
        }
        return true;
    }

    private NpadButton GetDebugInputKey()
    {
        NpadButton buttons = 0;
        for (int i = 0; i < KEY_CODES.Length; ++i)
        {
            if (Input.GetKey(KEY_CODES[i])) buttons |= mDebugKeyConfig.GetValue(KEY_CODES[i], (NpadButton)0);
        }
        return buttons;
    }

    private AnalogStickState GetDebugInputLAxis()
    {
        AnalogStickState state;
        state.x = (int)(Input.GetAxis("Horizontal") * AnalogStickState.Max);
        state.y = (int)(Input.GetAxis("Vertical") * AnalogStickState.Max);
        return state;
    }

    private AnalogStickState GetDebugInputRAxis()
    {
        AnalogStickState state = new AnalogStickState();
        if (Input.GetKey(KeyCode.I)) state.y = AnalogStickState.Max;
        if (Input.GetKey(KeyCode.J)) state.x = -AnalogStickState.Max;
        if (Input.GetKey(KeyCode.K)) state.y = -AnalogStickState.Max;
        if (Input.GetKey(KeyCode.L)) state.x = AnalogStickState.Max;
        return state;
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to)
    {
        Dispose();
    }
}
