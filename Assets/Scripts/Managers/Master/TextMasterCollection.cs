﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMasterCollection : IMasterCollection
{
    private Dictionary<int, TextMasterModel> mTexts = new Dictionary<int, TextMasterModel>();
    public string GetText(int text_id)
    {
        TextMasterModel text = mTexts.GetValue(text_id);
        return (text == null) ? "" : text.Text;
    }

    public int GetVoiceId(int text_id)
    {
        TextMasterModel text = mTexts.GetValue(text_id);
        return (text == null) ? -1 : text.VoiceId;
    }

    public void Setup()
    {
        mTexts.Clear();
        if (CCommonUIText.IsSetupEnd()) CCommonUIText.Clear();
        CCommonUIText.Setup(Resources.Load<TextAsset>("Binary/Text/CommonUIText"));
        CCommonUIText.SCommonUIText[] common_ui_texts = CCommonUIText.mCommonUITextStructDic.ValueToArray();
        for (int i = 0; i < common_ui_texts.Length; ++i)
        {
            TextMasterModel text = new TextMasterModel(common_ui_texts[i]);
            AddText(text);
        }

        if (CUIText.IsSetupEnd()) CUIText.Clear();
        CUIText.Setup(Resources.Load<TextAsset>("Binary/Text/UIText"));
        CUIText.SUIText[] ui_texts = CUIText.mUITextStructDic.ValueToArray();
        for (int i = 0; i < ui_texts.Length; ++i)
        {
            TextMasterModel text = new TextMasterModel(ui_texts[i]);
            AddText(text);
        }
    }

    public bool ShouldSetup()
    {
        return (!CCommonUIText.IsSetupEnd() || !CUIText.IsSetupEnd());
    }

    private void AddText(TextMasterModel text)
    {
        if (mTexts.ContainsKey(text.Id)) return;
        mTexts.Add(text.Id, text);
    }
}
