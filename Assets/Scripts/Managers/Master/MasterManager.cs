﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterManager : SingletonMonoBehaviour<MasterManager>, IDontDestroy
{
    private TextMasterCollection mTextMaster = new TextMasterCollection();
    public TextMasterCollection TextMaster { get { return mTextMaster; } }

    private IMasterCollection[] mCollections;
    private IMasterCollection[] Collections
    {
        get
        {
            if (mCollections == null)
            {
                mCollections = new IMasterCollection[]
                {
                    mTextMaster,
                };
            }
            return mCollections;
        }
    }

    public void SetupMasters()
    {
        for (int i = 0; i < Collections.Length; ++i)
        {
            if (Collections[i].ShouldSetup()) Collections[i].Setup();
        }
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
}
