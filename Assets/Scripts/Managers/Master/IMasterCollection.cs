﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMasterCollection
{
    /// <summary>
    /// マスターデータのセットアップ
    /// </summary>
    void Setup();

    /// <summary>
    /// セットアップする必要があるかどうか
    /// </summary>
    /// <returns>[true:セットアップする必要がある][false:セットアップする必要はない]</returns>
    bool ShouldSetup();
}
