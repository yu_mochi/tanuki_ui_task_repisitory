﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserManager : SingletonMonoBehaviour<UserManager>, IDontDestroy
{
    private UserModel mUser;
    public UserModel User { get { return mUser; } }

    private SaveTitleModel mSaveTitle;
    public SaveTitleModel SaveTitle { get { return mSaveTitle; } }

    protected override void Awake()
    {
        base.Awake();
        MasterManager.Instance.SetupMasters();

        // ☆サンプルだから適当に作るよ
        mUser = new UserModel();
        mUser.Farm.SetName("のび太");

        if (SaveDataManager.Instance.Load<SaveTitleModel>(out mSaveTitle, Define.System.Save.TITLE_FILE_NAME) != SaveDataManager.Result.Success)
        {
            mSaveTitle = new SaveTitleModel();
        }
    }

    /// <summary>
    /// ロードを試みる
    /// </summary>
    /// <param name="slot">セーブスロット</param>
    /// <returns>[true:成功][false:失敗]</returns>
    public bool TryLoadUserModel(Define.System.Save.SlotEnum slot)
    {
        string file_name = Define.System.Save.SlotToFileName(slot);
        if (string.IsNullOrEmpty(file_name)) return false;

        if (SaveDataManager.Instance.Load<UserModel>(out mUser, file_name) != SaveDataManager.Result.Success)
        {
            Debug.LogWarning("load failed !!!");
            return false;
        }

        mSaveTitle.UpdateLatestSlot(slot);
        return true;
    }

    /// <summary>
    /// セーブを試みる
    /// </summary>
    /// <param name="slot">セーブスロット</param>
    /// <param name="is_game_clear">ゲームをクリアしたかどうか</param>
    /// <returns>[true:成功][false:失敗]</returns>
    public bool TrySaveUserModel(Define.System.Save.SlotEnum slot, bool is_game_clear)
    {
        string file_name = Define.System.Save.SlotToFileName(slot);
        if (string.IsNullOrEmpty(file_name)) return false;

        if (SaveDataManager.Instance.Save<UserModel>(mUser, file_name) != SaveDataManager.Result.Success)
        {
            Debug.LogWarning("save failed !!!");
            return false;
        }

        // ゲームクリア直後フラグの設定
        mSaveTitle.SetIsAfterGameCleared(is_game_clear);

        mSaveTitle.Overwrite(slot, User.Farm.Name, User.Time, System.DateTime.Now, User.PlayTime, is_game_clear || mUser.IsGameCleared);
        if (SaveDataManager.Instance.Save<SaveTitleModel>(mSaveTitle, Define.System.Save.TITLE_FILE_NAME) != SaveDataManager.Result.Success)
        {
            Debug.LogWarning("title save failed !!!");
        }
        return true;
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
}
