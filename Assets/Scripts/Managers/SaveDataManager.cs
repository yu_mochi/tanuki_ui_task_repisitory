﻿using System.IO;
using UnityEngine;

public class SaveDataManager : SingletonMonoBehaviour<SaveDataManager>, IDontDestroy
{
    private const string CRYPTED_SIGN = "crypted";

    public enum Result
    {
        Success,
        Failure,
        NotFound,
        NotInitalized,
    }

    private bool mIsInitialized = false;

#if UNITY_SWITCH && !UNITY_EDITOR
    private nn.account.Uid mUserId;
    private nn.fs.FileHandle mFileHandle = new nn.fs.FileHandle();
    private const string mMountName = "MySave";
    private const int mSaveDataVersion = 1;
#endif

    #region AccessMethod
    /// <summary>
    /// セーブ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="save_data"></param>
    /// <param name="file_name"></param>
    /// <param name="crypt"></param>
    /// <returns></returns>
    public Result Save<T>(T save_data, string file_name, bool crypt = false)
    {
        if (mIsInitialized == false)
        {
            return Result.NotInitalized;
        }

        string data_json = JsonUtility.ToJson(save_data);

#if UNITY_EDITOR
        string file_path = string.Format("{0}/{1}.json", Application.persistentDataPath, file_name);

        Result result = Result.Failure;
        using (FileStream file_stream = new FileStream(file_path, FileMode.Create, FileAccess.Write))
        using (BinaryWriter writer = new BinaryWriter(file_stream))
        {
            writer.Write(data_json);
            writer.Close();
            file_stream.Close();
            result = Result.Success;
        }
        return result;

#elif UNITY_SWITCH
        string file_path = string.Format("{0}:/{1}", mMountName, file_name);

        byte[] data_byte = System.Text.Encoding.UTF8.GetBytes(data_json);

        int data_length = data_byte.Length + sizeof(int) * 2;

        byte[] data;
        using (MemoryStream stream = new MemoryStream(data_length))
        {
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(mSaveDataVersion);
            writer.Write(data_byte.Length);
            writer.Write(data_byte);
            stream.Close();
            data = stream.GetBuffer();
        }

        // Nintendo Switch Guideline 0080
        UnityEngine.Switch.Notification.EnterExitRequestHandlingSection();

        nn.Result result = nn.fs.File.Delete(file_path);
        if (!nn.fs.FileSystem.ResultPathNotFound.Includes(result))
        {
            if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();
        }

        result = nn.fs.File.Create(file_path, data_length);
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        result = nn.fs.File.Open(ref mFileHandle, file_path, nn.fs.OpenFileMode.Write);
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        result = nn.fs.File.Write(mFileHandle, 0, data, data.LongLength, nn.fs.WriteOption.Flush);
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        nn.fs.File.Close(mFileHandle);
        result = nn.fs.SaveData.Commit(mMountName);
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        // Nintendo Switch Guideline 0080
        UnityEngine.Switch.Notification.LeaveExitRequestHandlingSection();

        return Result.Success;
#else
        return Result.Failure;
#endif
    }

    /// <summary>
    /// ロード
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="load_data"></param>
    /// <param name="file_name"></param>
    /// <returns>Result</returns>
    public Result Load<T>(out T load_data, string file_name)
    {
        load_data = default(T);

        if (mIsInitialized == false)
        {
            return Result.NotInitalized;
        }

#if UNITY_EDITOR
        string file_path = string.Format("{0}/{1}.json", Application.persistentDataPath, file_name);

        if (File.Exists(file_path) == false)
        {
            return Result.NotFound;
        }

        Result result = Result.Failure;
        using (FileStream file_stream = new FileStream(file_path, FileMode.Open, FileAccess.Read))
        using (BinaryReader reader = new BinaryReader(file_stream))
        {
            string data_json = reader.ReadString();
            reader.Close();
            file_stream.Close();

            load_data = JsonUtility.FromJson<T>(data_json);

            result = Result.Success;
        }
        return result;

#elif UNITY_SWITCH
        string file_path = string.Format("{0}:/{1}", mMountName, file_name);

        nn.fs.EntryType entry_type = 0;
        nn.Result result = nn.fs.FileSystem.GetEntryType(ref entry_type, file_path);
        if (nn.fs.FileSystem.ResultPathNotFound.Includes(result)) return Result.NotFound;
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        result = nn.fs.File.Open(ref mFileHandle, file_path, nn.fs.OpenFileMode.Read);
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        long file_size = 0;
        result = nn.fs.File.GetSize(ref file_size, mFileHandle);
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        byte[] data = new byte[file_size];
        result = nn.fs.File.Read(mFileHandle, 0, data, file_size);
        if (!result.IsSuccess()) return Result.Failure; // result.abortUnlessSuccess();

        nn.fs.File.Close(mFileHandle);

        using (MemoryStream stream = new MemoryStream(data))
        {
            BinaryReader reader = new BinaryReader(stream);

            int version = reader.ReadInt32();
            Debug.Assert(version == mSaveDataVersion); // Save data version up
            int length = reader.ReadInt32();
            byte[] data_byte = reader.ReadBytes(length);
            string data_json = System.Text.Encoding.UTF8.GetString(data_byte);

            load_data = JsonUtility.FromJson<T>(data_json);
        }

        return Result.Success;
#else
        return Result.Failure;
#endif
    }

    /// <summary>
    /// 削除
    /// </summary>
    /// <param name="file_name"></param>
    /// <returns></returns>
    public Result Delete(string file_name)
    {
        if (mIsInitialized == false)
        {
            return Result.NotInitalized;
        }

#if UNITY_EDITOR
        string file_path = string.Format("{0}/{1}.json", Application.persistentDataPath, file_name);

        if (File.Exists(file_path) == false)
        {
            return Result.NotFound;
        }

        File.Delete(file_path);

        return Result.Success;

#elif UNITY_SWITCH
        string file_path = string.Format("{0}:/{1}", mMountName, file_name);

        nn.Result result = nn.fs.File.Delete(file_path);
        if (nn.fs.FileSystem.ResultPathNotFound.Includes(result))
        {
            return Result.NotFound;
        }
        else if (!result.IsSuccess())
        {
            return Result.Failure;
        }
        return Result.Success;
#else
        return Result.Failure;
#endif
    }

    /// <summary>
    /// ファイル存在チェック
    /// </summary>
    /// <param name="file_name"></param>
    /// <returns></returns>
    public bool IsExist(string file_name)
    {
        if (mIsInitialized == false)
        {
            return false;
        }

#if UNITY_EDITOR
        string file_path = string.Format("{0}/{1}.json", Application.persistentDataPath, file_name);

        if (File.Exists(file_path) == false)
        {
            return false;
        }
        return true;

#elif UNITY_SWITCH
        string file_path = string.Format("{0}:/{1}", mMountName, file_name);

        nn.fs.EntryType entry_type = 0;
        nn.Result result = nn.fs.FileSystem.GetEntryType(ref entry_type, file_path);
        if (!result.IsSuccess() || nn.fs.FileSystem.ResultPathNotFound.Includes(result))
        {
            return false;
        }
        return true;
#else
        return false;
#endif
    }

    /// <summary>
    /// ファイルがひとつでも存在するかチェック
    /// </summary>
    /// <returns></returns>
    public bool IsExist()
    {
        for (int i = 0; i < (int)Define.System.Save.SlotEnum.Length; i++)
        {
            if (IsExist(Define.System.Save.SlotToFileName((Define.System.Save.SlotEnum)i)))
            {
                return true;
            }
        }
        return false;
    }

    #endregion

    #region UnityMethod
    override protected void Awake()
    {
        Initialize();
    }

    override protected void OnDestroy()
    {
        Dispose();

        base.OnDestroy();
    }
    #endregion

    #region SystemMethod
    private void Initialize()
    {
#if UNITY_SWITCH && !UNITY_EDITOR
        nn.account.Account.Initialize();
        nn.account.UserHandle user_handle = new nn.account.UserHandle();

        nn.account.Account.OpenPreselectedUser(ref user_handle);
        nn.account.Account.GetUserId(ref mUserId, user_handle);

        nn.Result result = nn.fs.SaveData.Mount(mMountName, mUserId);
        result.abortUnlessSuccess();
#endif
        mIsInitialized = true;
    }

    private void Dispose()
    {
#if UNITY_SWITCH && !UNITY_EDITOR
        nn.fs.FileSystem.Unmount(mMountName);
#endif
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    #endregion
}
