﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeManager : SingletonMonoBehaviour<FadeManager>, IDontDestroy
{
    private enum State
    {
        FadeOut,
        FadeIn,
    }

    [SerializeField]
    private GameObject mRoot;
    [SerializeField]
    private Image mImage;

    private State mState = State.FadeIn;
    private FadeCalculator mFadeCalculator = new FadeCalculator();
    private System.Action mEndedCallback;
    private bool mIsFading;

    #region AccessMethod
    /// <summary>
    /// 黒のフェードアウト(透明度0→1)
    /// </summary>
    /// <param name="fade_time">フェードに要する時間</param>
    /// <param name="ended_callback">フェード終了時に呼ばれるコールバック</param>
    public void FadeOut(float fade_time, System.Action ended_callback = null) { Begin(State.FadeOut, Color.black, 0.0f, 1.0f, fade_time, ended_callback); }

    /// <summary>
    /// 黒のフェードイン(透明度1→0)
    /// </summary>
    /// <param name="fade_time">フェードに要する時間</param>
    /// <param name="ended_callback">フェード終了時に呼ばれるコールバック</param>
    public void FadeIn(float fade_time, System.Action ended_callback = null) { Begin(State.FadeIn, Color.black, 1.0f, 0.0f, fade_time, ended_callback); }

    /// <summary>
    /// 白のフェードアウト(透明度0→1)
    /// </summary>
    /// <param name="fade_time">フェードに要する時間</param>
    /// <param name="ended_callback">フェード終了時に呼ばれるコールバック</param>
    public void WhiteOut(float fade_time, System.Action ended_callback = null) { Begin(State.FadeOut, Color.white, 0.0f, 1.0f, fade_time, ended_callback); }

    /// <summary>
    /// 白のフェードイン(透明度1→0)
    /// </summary>
    /// <param name="fade_time">フェードに要する時間</param>
    /// <param name="ended_callback">フェード終了時に呼ばれるコールバック</param>
    public void WhiteIn(float fade_time, System.Action ended_callback = null) { Begin(State.FadeIn, Color.white, 1.0f, 0.0f, fade_time, ended_callback); }
    #endregion

    #region UnityMethod
    protected override void Awake()
    {
        base.Awake();
        mRoot.Deactivate();
    }

    private void Update()
    {
        if (!mIsFading) return;
        SetAlpha(mFadeCalculator.CalculateValue(Time.deltaTime));
        if (mFadeCalculator.IsEnd()) End();
    }
    #endregion

    #region SystemMethod
    private void SetColor(Color color) { if (mImage != null) mImage.color = color; }
    private void SetAlpha(float alpha) { if (mImage != null) mImage.color = new Color(mImage.color.r, mImage.color.g, mImage.color.b, alpha); }
    private void Begin(State state, Color color, float start_alpha, float target_alpha, float fade_time, System.Action ended_callback)
    {
        if (mIsFading || mState == state)
        {
            Debug.Log("Fade 切り替え呼び出しが重複しています."); // 邪魔だったら消してね   2018/04/10 Kadomoto.
            if (ended_callback != null) ended_callback();
            return;
        }
        SetColor(color);
        SetAlpha(start_alpha);
        mRoot.Activate();
        mFadeCalculator.Setup(start_alpha, target_alpha, fade_time);
        mState = state;
        mEndedCallback = ended_callback;
        mIsFading = true;
    }

    private void End()
    {
        mIsFading = false;

        // フェードインが終了した時はRoot自体を非アクティブにしておく
        if (mState == State.FadeIn) mRoot.Deactivate();
        if (mEndedCallback != null) mEndedCallback();
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    #endregion
}
