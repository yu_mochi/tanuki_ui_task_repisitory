﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : SingletonMonoBehaviour<SceneManager>, IDontDestroy
{
    [SerializeField]
    private SceneController[] mScenes;
    private readonly Define.Scene.NameEnum mFirstScene = Define.Scene.NameEnum.UI;

    private SceneController mActiveScene;
    private SceneController mNextScene;
    public Define.Scene.NameEnum Scene
    {
        get { return GetSceneName(mActiveScene); }
    }

    private Define.Scene.NameEnum mFromScene = Define.Scene.NameEnum.None;
    public Define.Scene.NameEnum FromScene
    {
        get { return this.mFromScene; }
    }

    private static IDontDestroy[] mManagers;
    private static IDontDestroy[] Managers
    {
        get
        {
            if (mManagers == null)
            {
                GameObject obj = GameObject.Find("ManagerCollection");
                mManagers = obj.GetComponentsInChildren<IDontDestroy>();
            }
            return mManagers;
        }
    }

    private static IDontDestroy[] mFactories;
    private static IDontDestroy[] Factories
    {
        get
        {
            if (mFactories == null)
            {
                GameObject obj = GameObject.Find("FactoryCollection");
                mFactories = obj.GetComponentsInChildren<IDontDestroy>();
            }
            return mFactories;
        }
    }

    [SerializeField]
    private float mFadeTime = 0.5f;
    private System.Action mDisposeCallback;
    private System.Action mUpdatedCallback;
    private int mRequestCount;
    private IMultipleLoadHelper mLoadHelper;

    private Define.Scene.NameEnum mQueueingScene = Define.Scene.NameEnum.None;

    #region AccessMethod
    /// <summary>
    /// サンプル用UIシーンへ遷移
    /// </summary>
    /// <param name="slot">ロードするセーブデータのスロット</param>
    public void GoToUIScene(Define.System.Save.SlotEnum slot = Define.System.Save.SlotEnum.None)
    {
        UISceneController scene = mScenes[(int)Define.Scene.NameEnum.UI] as UISceneController;
        GoToNext(scene, slot);
    }
    #endregion

    #region UnityMethod
    private void Start()
    {
        mActiveScene = mScenes[(int)mFirstScene];
        mActiveScene.Initialize();
        BeginTransition(Define.System.Save.SlotEnum.None);
    }

    private void LateUpdate()
    {
        if (mUpdatedCallback != null)
        {
            mUpdatedCallback();
        }

        if (mDisposeCallback != null)
        {
            System.Action action = mDisposeCallback;
            mDisposeCallback = null;
            Dispose(action);
        }

        if (UserManager.Instance.User != null)
        {
            UserManager.Instance.User.AddPlayTime(Time.deltaTime);
        }
    }
    #endregion

    #region SystemMethod
    /// <summary>
    /// シーン名を取得
    /// </summary>
    /// <param name="scene">シーンのController</param>
    /// <returns>シーン名のenum</returns>
    private Define.Scene.NameEnum GetSceneName(SceneController scene)
    {
        if (scene != null)
        {
            if (scene is UISceneController) return Define.Scene.NameEnum.UI;
        }
        return Define.Scene.NameEnum.None;
    }

    /// <summary>
    /// 次のシーンに遷移する
    /// </summary>
    /// <param name="scene">遷移先のシーンのController</param>
    /// <param name="slot">ロードするセーブデータのスロット</param>
    private void GoToNext(SceneController scene, Define.System.Save.SlotEnum slot)
    {
        if (scene == null || mNextScene != null) return;
        DisableInput();
        mFromScene = Scene;
        mNextScene = scene;
        mNextScene.Initialize();
        BeginTransition(slot);
    }

    /// <summary>
    /// 遷移開始
    /// </summary>
    /// <param name="slot">ロードするセーブデータのスロット</param>
    private void BeginTransition(Define.System.Save.SlotEnum slot)
    {
        if (mNextScene == null)
        {
            // 次のシーンが設定されていない＝最初のシーン
            LoadData(mActiveScene, true, slot);
            return;
        }

        FadeOut(() =>
        {
            mActiveScene.DestroyScene();
            mDisposeCallback = () =>
            {
                mActiveScene = mNextScene;
                mNextScene = null;
                LoadData(mActiveScene, false, slot);
            };
        });
    }

    /// <summary>
    /// データをロードする
    /// </summary>
    /// <param name="scene">シーンのController</param>
    /// <param name="is_first_scene">最初のシーンかどうか</param>
    /// <param name="slot">ロードするセーブデータのスロット</param>
    private void LoadData(SceneController scene, bool is_first_scene, Define.System.Save.SlotEnum slot)
    {
        // ロードするセーブデータのスロットが指定されていた場合は,
        // シーン関連のロードが始まる直前のこのタイミングでロードする
        UserManager.Instance.TryLoadUserModel(slot);

        mLoadHelper = scene.LoadData(() =>
        {
            SetupCommonManagementObjects();
            scene.CreateManagementObjects();

            CreateScene(!is_first_scene);
        });

        if (mLoadHelper == null) return;
        mRequestCount = mLoadHelper.RequestCount;
        mUpdatedCallback = () =>
        {
            if (mLoadHelper == null) return;
            // ☆ほんとはロード進捗の更新とかしてたよ
        };
    }

    /// <summary>
    /// 常駐Manager及びFactoryのセットアップ
    /// </summary>
    private void SetupCommonManagementObjects()
    {
        if (Factories != null)
        {
            for (int i = 0; i < Factories.Length; ++i)
            {
                IDontDestroy f = Factories[i];
                if (f == null) continue;
                f.CreateAtScene(FromScene, Scene);
            }
        }
        if (Managers != null)
        {
            for (int i = 0; i < Managers.Length; ++i)
            {
                IDontDestroy m = Managers[i];
                if (m == null) continue;
                m.CreateAtScene(FromScene, Scene);
            }
        }
    }

    /// <summary>
    /// シーンを生成する
    /// </summary>
    /// <param name="with_fade_in">フェードインするかどうか</param>
    private void CreateScene(bool with_fade_in)
    {
        System.Action action = () =>
        {
            mActiveScene.FixedCreate();
            EnableInput();
        };

        mActiveScene.CreateScene(() =>
        {
            if (with_fade_in)
            {
                FadeIn(action);
                return;
            }
            action();
        });

        mRequestCount = 0;
        mLoadHelper = null;
        mUpdatedCallback = null;
    }

    /// <summary>
    /// フェードアウト
    /// </summary>
    /// <param name="end_callback">フェード完了時に呼ばれるコールバック</param>
    private void FadeOut(System.Action end_callback)
    {
        FadeManager.Instance.FadeOut(mFadeTime, () =>
        {
            end_callback();
        });
    }

    /// <summary>
    /// フェードイン
    /// </summary>
    /// <param name="end_callback">フェード完了時に呼ばれるコールバック</param>
    private void FadeIn(System.Action end_callback)
    {
        FadeManager.Instance.FadeIn(mFadeTime, () =>
        {
            end_callback();
        });
    }

    /// <summary>
    /// 入力許可
    /// </summary>
    private void EnableInput()
    {
        InputManager.Instance.EnableInput();
    }

    /// <summary>
    /// 入力禁止
    /// </summary>
    private void DisableInput()
    {
        InputManager.Instance.DisableInput();
    }

    /// <summary>
    /// シーン終了時の処理
    /// </summary>
    /// <param name="end_callback"></param>
    private void Dispose(System.Action end_callback)
    {
        if (Factories != null)
        {
            for (int i = 0; i < Factories.Length; ++i)
            {
                IDontDestroy f = Factories[i];
                if (f == null) continue;
                f.DisposeAtScene(FromScene, GetSceneName(mNextScene));
            }
        }

        if (Managers != null)
        {
            for (int i = 0; i < Managers.Length; ++i)
            {
                IDontDestroy m = Managers[i];
                if (m == null) continue;
                m.DisposeAtScene(FromScene, GetSceneName(mNextScene));
            }
        }

        // 最後にメモリをクリア
        StartCoroutine(SystemUtility.UnloadUnusedAssets(() =>
        {
            System.GC.Collect();
            if (end_callback != null) end_callback();
        }));
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    #endregion
}
