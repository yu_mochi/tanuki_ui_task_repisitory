﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : SingletonMonoBehaviour<UIManager>, IDontDestroy
{
    [SerializeField]
    private Transform mCanvas;
    [SerializeField]
    private Camera mUICamera;

    private Dictionary<Define.UI.TypeEnum, UIController>[] mUITables = new Dictionary<Define.UI.TypeEnum, UIController>[(int)Define.Scene.NameEnum.Length];
    private Stack<UIController>[] mUIStacks = new Stack<UIController>[(int)Define.Scene.NameEnum.Length];
    private Dictionary<Define.UI.TypeEnum, UIController> ActiveUITable
    {
        get
        {
            int active_scene = (int)SceneManager.Instance.Scene;
            if (mUITables[active_scene] == null)
            {
                mUITables[active_scene] = new Dictionary<Define.UI.TypeEnum, UIController>();
            }
            return mUITables[active_scene];
        }
    }
    private Stack<UIController> ActiveUIStack
    {
        get
        {
            int active_scene = (int)SceneManager.Instance.Scene;
            if (mUIStacks[active_scene] == null)
            {
                mUIStacks[active_scene] = new Stack<UIController>();
            }
            return mUIStacks[active_scene];
        }
    }

    // シーン別UI破棄フラグ
    private bool[] mIsUIDestroy = new bool[(int)Define.Scene.NameEnum.Length] { true };
    public bool IsActiveUIDestroy { get { return mIsUIDestroy[(int)SceneManager.Instance.Scene]; } set { mIsUIDestroy[(int)SceneManager.Instance.Scene] = value; } }

    private int mEnabledInputCount;

    private bool mIsOpenReady = false;
    private UIController mWaitOpenUI = null;

    #region AccessMethod
    /// <summary>
    /// UIを開く
    /// </summary>
    /// <param name="type">UIのタイプ</param>
    /// <param name="arg">初期化用のパラメータ</param>
    /// <returns>制御スクリプト</returns>
    public UIController Open(Define.UI.TypeEnum type, UIArgument arg = null)
    {

        UIController controller = ActiveUITable.GetValue(type);
        if (controller != null)
        {
            Debug.LogWarning(type + "already exists !!!");
            return controller;
        }

        controller = UIFactory.Instance.Create(type, mCanvas);
        Debug.Assert(controller != null, "UIController is null !!!");
        if (controller != null)
        {
            DisableInput();
            controller.Initialize(arg);
            controller.Type = type;

            ActiveUITable.Add(type, controller);
            ActiveUIStack.Push(controller);

            mIsOpenReady = false;
            mWaitOpenUI = controller;
            mWaitOpenUI.Deactivate();
        }
        return controller;
    }

    /// <summary>
    /// 一番上のUIを閉じる
    /// </summary>
    /// <param name="destroyed_callback">UIが破棄された時に呼ばれるコールバック</param>
    public void Pop(System.Action destroyed_callback = null)
    {
        if (ActiveUIStack.Count > 0)
        {
            UIController controller = ActiveUIStack.Pop();

            // 開き待機中のUIを閉じる場合は,待機と入力制限の解除を行う
            if (mWaitOpenUI != null && mWaitOpenUI.Type == controller.Type)
            {
                EnableInput();
                mWaitOpenUI = null;
            }

            // 開き中のUIを閉じようとした時は入力制限を行わない
            // ※開ききった時に呼ばれる筈の入力制限解除が呼ばれない分余計に入力制限が掛かってしまう為
            if (!controller.IsOpening) DisableInput();

            controller.Close((type) =>
            {
                DestroyUI(type);
                if (destroyed_callback != null) destroyed_callback();
            });
        }
    }

    /// <summary>
    /// 一番下のUI以外を全て閉じる
    /// </summary>
    /// <param name="destroyed_callback">全てのUIが破棄された時に呼ばれるコールバック</param>
    public void PopExceptForBottom(System.Action destroyed_callback = null)
    {
        if (ActiveUIStack.Count <= 1)
        {
            if (destroyed_callback != null) destroyed_callback();
            return;
        }

        Dictionary<Define.UI.TypeEnum, UIController> table = new Dictionary<Define.UI.TypeEnum, UIController>();
        int count = ActiveUIStack.Count - 1;
        for (int i = 0; i < count; ++i)
        {
            UIController controller = ActiveUIStack.Pop();

            // 開き待機中のUIを閉じる場合は,待機と入力制限の解除を行う
            if (mWaitOpenUI != null && mWaitOpenUI.Type == controller.Type)
            {
                EnableInput();
                mWaitOpenUI = null;
            }

            table.Add(controller.Type, controller);
        }

        UIController[] controllers = table.ValueToArray();
        for (int i = 0; i < controllers.Length; ++i)
        {
            // 開き中のUIを閉じようとした時は入力制限を行わない
            // ※開ききった時に呼ばれる筈の入力制限解除が呼ばれない分余計に入力制限が掛かってしまう為
            if (!controllers[i].IsOpening) DisableInput();

            controllers[i].Close((type) =>
            {
                DestroyUI(type);
                table.Remove(type);
                if (table.Count == 0 && destroyed_callback != null) destroyed_callback();
            });
        }
    }

    /// <summary>
    /// UIの制御スクリプトを取得
    /// </summary>
    /// <param name="type">UIのタイプ</param>
    /// <returns>制御スクリプト</returns>
    public UIController GetUIController(Define.UI.TypeEnum type)
    {
        return ActiveUITable.GetValue(type);
    }

    /// <summary>
    /// UI用カメラを取得
    /// </summary>
    /// <returns>UI用カメラ</returns>
    public Camera GetUICamera()
    {
        return mUICamera;
    }

    /// <summary>
    /// 入力許可
    /// </summary>
    public void EnableInput()
    {
        --mEnabledInputCount;
        if (mEnabledInputCount < 0) mEnabledInputCount = 0;
    }

    /// <summary>
    /// 入力禁止
    /// </summary>
    public void DisableInput()
    {
        ++mEnabledInputCount;
    }

    /// <summary>
    /// 入力可能かどうか
    /// </summary>
    /// <returns>[true:入力可能][false:入力不可]</returns>
    public bool CanInput()
    {
        return (mEnabledInputCount == 0);
    }

    /// <summary>
    /// スタックしているUIの数を取得
    /// </summary>
    /// <returns>スタックしているUIの数</returns>
    public int GetUIStackCount()
    {
        return ActiveUIStack.Count;
    }
    #endregion

    #region SystemMethod
    /// <summary>
    /// 指定したUIを解放
    /// </summary>
    /// <param name="type">UIのタイプ</param>
    private void DestroyUI(Define.UI.TypeEnum type)
    {
        UIController controller = ActiveUITable.GetValue(type);

        if (controller != null)
        {
            ActiveUITable.Remove(type);
            controller.Destroy();
            GameObject.Destroy(controller.gameObject);
        }
        EnableInput();
    }

    /// <summary>
    /// 全てのUIを解放
    /// </summary>
    private void DestroyAllUI()
    {
        UIController[] controllers = ActiveUITable.ValueToArray();
        if (controllers == null) return;

        for (int i = 0; i < controllers.Length; ++i)
        {
            if (controllers[i] != null)
            {
                controllers[i].Destroy();
                GameObject.Destroy(controllers[i].gameObject);
                controllers[i] = null;
            }
        }
        ActiveUITable.Clear();
        ActiveUIStack.Clear();
    }

    /// <summary>
    /// 削除ではなく非アクティブ化
    /// </summary>
    private void DeactivateAllUI()
    {
        UIController[] controllers = ActiveUITable.ValueToArray();
        if (controllers == null) return;

        for (int i = 0; i < controllers.Length; ++i)
        {
            if (controllers[i] != null)
            {
                controllers[i].Deactivate();
            }
        }
    }

    private void ActivateAllUI()
    {
        UIController[] controllers = ActiveUITable.ValueToArray();
        if (controllers == null) return;

        for (int i = 0; i < controllers.Length; ++i)
        {
            if (controllers[i] != null)
            {
                controllers[i].Activate();
            }
        }
    }

    /// <summary>
    /// 入力イベントを登録する
    /// </summary>
    private void RegisterInputEvent()
    {
        InputManager.Instance.InputButtonDownCallback += InputButtonDownCallback;
        InputManager.Instance.InputButtonUpCallback += InputButtonUpCallback;
        InputManager.Instance.InputButtonCallback += InputButtonCallback;
        InputManager.Instance.InputAnalogStickLCallback += InputAnalogStickLCallback;
        InputManager.Instance.InputAnalogStickLReleaseCallback += InputAnalogStickLReleaseCallback;
    }

    /// <summary>
    /// 入力イベントの登録を解除する
    /// </summary>
    private void UnregisterInputEvent()
    {
        InputManager.Instance.InputButtonDownCallback -= InputButtonDownCallback;
        InputManager.Instance.InputButtonUpCallback -= InputButtonUpCallback;
        InputManager.Instance.InputButtonCallback -= InputButtonCallback;
        InputManager.Instance.InputAnalogStickLCallback -= InputAnalogStickLCallback;
        InputManager.Instance.InputAnalogStickLReleaseCallback -= InputAnalogStickLReleaseCallback;
    }

    /// <summary>
    /// ボタンが押された時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">押されたボタンのビットフラグ</param>
    private void InputButtonDownCallback(nn.hid.NpadButton buttons)
    {
        if (!CanInput()) return;
        if (ActiveUIStack.Count > 0) ActiveUIStack.Peek().InputButtonDownCallback(buttons);
    }

    /// <summary>
    /// ボタンが離された時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">離されたボタンのビットフラグ</param>
    private void InputButtonUpCallback(nn.hid.NpadButton buttons)
    {
        if (!CanInput()) return;
        if (ActiveUIStack.Count > 0) ActiveUIStack.Peek().InputButtonUpCallback(buttons);
    }

    /// <summary>
    /// ボタンが押されている時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">押されているボタンのビットフラグ</param>
    private void InputButtonCallback(nn.hid.NpadButton buttons)
    {
        if (!CanInput()) return;
        if (ActiveUIStack.Count > 0) ActiveUIStack.Peek().InputButtonCallback(buttons);
    }

    /// <summary>
    /// 左スティックに入力がある時に呼ばれるコールバック
    /// </summary>
    /// <param name="state">入力状態</param>
    private void InputAnalogStickLCallback(nn.hid.AnalogStickState state)
    {
        if (!CanInput()) return;
        if (ActiveUIStack.Count > 0) ActiveUIStack.Peek().InputAnalogStickLCallback(state);
    }

    /// <summary>
    /// 左スティックが離された際に呼ばれるコールバック
    /// </summary>
    private void InputAnalogStickLReleaseCallback()
    {
        if (!CanInput()) return;
        if (ActiveUIStack.Count > 0) ActiveUIStack.Peek().InputAnalogStickLReleaseCallback();
    }
    #endregion

    private void Update()
    {
        if (mWaitOpenUI != null)
        {
            if (mIsOpenReady)
            {
                mWaitOpenUI.Activate();
                mWaitOpenUI.Open(EnableInput);
                mWaitOpenUI = null;
            }
            else
            {
                mIsOpenReady = true;
            }
        }
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to)
    {
        RegisterInputEvent();

        // UIを開いたままシーン遷移したケースを考慮して,
        // このタイミングで入力制限カウントを一旦強制的にリセットする
        mEnabledInputCount = 0;

        // 破棄していなかったUIは有効化
        if (!IsActiveUIDestroy)
        {
            ActivateAllUI();
        }
    }

    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to)
    {
        UnregisterInputEvent();

        if (IsActiveUIDestroy)
        {
            DestroyAllUI();
        }
        else
        {
            // 破棄しない場合は無効化
            DeactivateAllUI();
        }
        DisableInput();
    }
}
