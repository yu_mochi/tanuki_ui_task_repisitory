﻿/**
 *    バイナリ読み込みプログラム
 *    使い方:
 *    	初期化処理(Setup)後に
 *    	GetData()でデータを取得できます
 *    
 *    ※このプログラムは自動生成されました
*/
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class CUIText
{
	
	public struct SUIText
	{
		public int mId;
		public string mTextData;
	}
	
	// セットアップ終了フラグ
	private static bool mIsSetupEnd = false;
	public static int Length
	{
		get;
		private set;
	}
	
	// 読み込みデータ(Struct)
	public static Dictionary<int, SUIText> mUITextStructDic = null;
	
	// ID指定によるデータ取得(Struct)
	// データが存在しない場合はnullを返します
	public static SUIText GetData(int id)
	{
		SUIText data = new SUIText();
		if(!mUITextStructDic.TryGetValue(id, out data))
		{
			Debug.LogError("Error: ID["+id+"] not exist!");
			Debug.Break();
		}
		return data;
	}
	
	// セットアップ状態を返す
	public static bool IsSetupEnd()
	{
		return mIsSetupEnd;
	}
	
	// Dictionaryクリア
	public static void Clear()
	{
		mIsSetupEnd = false;
		mUITextStructDic.Clear();
		mUITextStructDic = null;
	}
	
	// データ読込は各自で行い、Setup関数の引数に読み込んだTextAssetを指定してください。
	// データ読込・格納(Struct)
	public static bool Setup(TextAsset textAsset)
	{
		if(!textAsset || mUITextStructDic != null)
		{
			return false;
		}
		mUITextStructDic = new Dictionary<int, SUIText>();
		
		using(MemoryStream binary = new MemoryStream(textAsset.bytes))
		using(BinaryReader reader = new BinaryReader(binary))
		{
			try
			{
				byte[] readBytes = null;
				int nBytes = -1;
				while(true)
				{
					SUIText _StorageData = new SUIText();
					_StorageData = new SUIText();
					
					nBytes = reader.ReadInt16();
					readBytes = reader.ReadBytes(nBytes);
					_StorageData.mId = Getint(readBytes);
					nBytes = reader.ReadInt16();
					readBytes = reader.ReadBytes(nBytes);
					_StorageData.mTextData = Getstring(readBytes);
					
					
					mUITextStructDic.Add(_StorageData.mId, _StorageData);
				}
			}
			catch(EndOfStreamException)
			{
				// 終端
			}
		}
		Length = mUITextStructDic.Count;
		mIsSetupEnd = true;
		return true;
	}
	
	// データ型変換
	private static System.Int32 Getint(byte[] readByte)
	{
		return BitConverter.ToInt32(readByte, 0);
	}
	
	private static System.String Getstring(byte[] readByte)
	{
		System.String data = System.Text.Encoding.UTF8.GetString(readByte);
		return (data == "\0" ? null : data);
	}
	
	private static System.Single Getfloat(byte[] readByte)
	{
		return (float)BitConverter.ToSingle(readByte, 0);
	}
	
	private static System.Double Getdouble(byte[] readByte)
	{
		return (double)BitConverter.ToDouble(readByte, 0);
	}
	
	private static System.Int16 Getshort(byte[] readByte)
	{
		return BitConverter.ToInt16(readByte, 0);
	}
	
	private static System.UInt16 Getushort(byte[] readByte)
	{
		return BitConverter.ToUInt16(readByte, 0);
	}
	
	private static System.Int32 Getenum(byte[] readByte)
	{
		return BitConverter.ToInt32(readByte, 0);
	}
	
	private static System.Int32 Getbit(byte[] readByte)
	{
		return BitConverter.ToInt32(readByte, 0);
	}
	
	private static System.Boolean Getbool(byte[] readByte)
	{
		return BitConverter.ToBoolean(readByte, 0);
	}
	
}
