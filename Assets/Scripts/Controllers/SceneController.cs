﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SceneController : MonoBehaviour
{
    [SerializeField]
    private string mContentsName;
    private GameObject mContents;
    public GameObject Contents { get { return mContents; } }

    [SerializeField]
    private string mManagerName;
    private GameObject mManager;

    [SerializeField]
    private string mFactoryName;
    private GameObject mFactory;

    private static readonly string CONTENTS_PATH = "Prefabs/Scene/Contents/";
    private static readonly string MANAGER_PATH = "Prefabs/Scene/Managers/";
    private static readonly string FACTORY_PATH = "Prefabs/Scene/Factories/";

    /// <summary>
    /// データのロード前に行う初期化
    /// </summary>
    public virtual void Initialize() { }

    /// <summary>
    /// シーンに必要なデータのロード
    /// </summary>
    /// <param name="end_callback">ロード終了時に呼ばれるコールバック</param>
    /// <returns>ロードヘルパーのインターフェース</returns>
    public virtual IMultipleLoadHelper LoadData(System.Action end_callback)
    {
        if (end_callback != null) end_callback();
        return null;
    }

    /// <summary>
    /// 管理者系オブジェクトを生成
    /// </summary>
    public virtual void CreateManagementObjects()
    {
        if (mManager == null && !string.IsNullOrEmpty(mManagerName))
        {
            mManager = Instantiate(Resources.Load<Object>(MANAGER_PATH + mManagerName)) as GameObject;
        }

        if (mFactory == null && !string.IsNullOrEmpty(mFactoryName))
        {
            mFactory = Instantiate(Resources.Load<Object>(FACTORY_PATH + mFactoryName)) as GameObject;
        }
    }

    /// <summary>
    /// シーンコンテンツを生成
    /// </summary>
    /// <param name="end_callback">生成終了時に呼ばれるコールバック</param>
    public virtual void CreateScene(System.Action end_callback)
    {
        if (mContents == null && !string.IsNullOrEmpty(mContentsName))
        {
            mContents = Instantiate(Resources.Load<Object>(CONTENTS_PATH + mContentsName)) as GameObject;
        }
        if (end_callback != null) end_callback();
    }

    /// <summary>
    /// シーン生成の全行程終了時の処理
    /// </summary>
    public virtual void FixedCreate() { }

    /// <summary>
    /// シーンの要素を全て削除
    /// </summary>
    public virtual void DestroyScene()
    {
        if (mContents != null)
        {
            Destroy(mContents);
            mContents = null;
        }

        if (mManager != null)
        {
            Destroy(mManager);
            mManager = null;
        }

        if (mFactory != null)
        {
            Destroy(mFactory);
            mFactory = null;
        }
    }
}
