﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadUIBehaviour : ISaveUIBehaviour
{
    /// <summary>
    /// 開くアニメーションのハッシュを取得
    /// </summary>
    /// <returns>開くアニメーションのハッシュ</returns>
    public int GetOpenAnimationHash() { return Animator.StringToHash("UI_SaveList_01_01"); }

    /// <summary>
    /// タイトル取得
    /// </summary>
    /// <returns>タイトル</returns>
    public string GetTitle() { return MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1200_0104); }

    /// <summary>
    /// クイックセーブデータのルートをactiveにすべきかどうか
    /// </summary>
    /// <returns>[true:activeにすべき][false:activeにすべきではない]</returns>
    public bool ShouldActivateQuickSaveRoot() { return true; }

    /// <summary>
    /// 先頭のカーソルインデックスを取得
    /// </summary>
    /// <returns>先頭のカーソルインデックス</returns>
    public int GetTopCursorIndex() { return 0; }

    /// <summary>
    /// 決定時の処理を実行する
    /// </summary>
    /// <param name="slot">セーブスロット</param>
    public void ExecuteDecisionProcess(Define.System.Save.SlotEnum slot) { /*ホントはシーン遷移とかしてた SceneManager.Instance.GoToFarm(slot);*/ }

    /// <summary>
    /// 決定時の処理が実行できるかどうか
    /// </summary>
    /// <param name="slot">セーブスロット</param>
    /// <returns>[true:実行できる][false:実行できない]</returns>
    public bool CanExecuteDecisionProcess(Define.System.Save.SlotEnum slot) { return SaveDataManager.Instance.IsExist(Define.System.Save.SlotToFileName(slot)); }

    /// <summary>
    /// 確認テキスト取得
    /// </summary>
    /// <returns>確認テキスト</returns>
    public string GetConfirmationText() { return MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0003); }
}
