﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaveUIBehaviour
{
    /// <summary>
    /// 開くアニメーションのハッシュを取得
    /// </summary>
    /// <returns>開くアニメーションのハッシュ</returns>
    int GetOpenAnimationHash();

    /// <summary>
    /// タイトル取得
    /// </summary>
    /// <returns>タイトル</returns>
    string GetTitle();

    /// <summary>
    /// クイックセーブデータのルートをactiveにすべきかどうか
    /// </summary>
    /// <returns>[true:activeにすべき][false:activeにすべきではない]</returns>
    bool ShouldActivateQuickSaveRoot();

    /// <summary>
    /// 先頭のカーソルインデックスを取得
    /// </summary>
    /// <returns>先頭のカーソルインデックス</returns>
    int GetTopCursorIndex();

    /// <summary>
    /// 決定時の処理を実行する
    /// </summary>
    /// <param name="slot">セーブスロット</param>
    void ExecuteDecisionProcess(Define.System.Save.SlotEnum slot);

    /// <summary>
    /// 決定時の処理が実行できるかどうか
    /// </summary>
    /// <param name="slot">セーブスロット</param>
    /// <returns>[true:実行できる][false:実行できない]</returns>
    bool CanExecuteDecisionProcess(Define.System.Save.SlotEnum slot);

    /// <summary>
    /// 確認テキスト取得
    /// </summary>
    /// <returns>確認テキスト</returns>
    string GetConfirmationText();
}
