﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InputAssistController
{
    // 継続入力の際の, 最初の待ち時間と継続時のインターバルを分別   2018/06/26 Kadomoto.
    private const float DEFAULT_FIRST_INPUT_INTERVAL = 0.4f;
    private const float DEFAULT_CONTINUOUS_INPUT_INTERVAL = 0.1f;

    private static readonly nn.hid.NpadButton[] NPAD_BUTTONS = Enum.GetValues(typeof(nn.hid.NpadButton)) as nn.hid.NpadButton[];

    private Dictionary<int, bool> mIsInputContinuedDic = new Dictionary<int, bool>(NPAD_BUTTONS.Length); // 各ボタンが入力継続状態か否かのフラグ  2018/06/26 Kadomoto.
    private Dictionary<int, float> mInputTimeDic = new Dictionary<int, float>(NPAD_BUTTONS.Length);

    private nn.hid.NpadButton mHoldButtons = nn.hid.NpadButton.None;

    private float mInputFirstInterval = 0;
    private float mInputContinuousInterval = 0;
    public float InputFirstInterval { get { return mInputFirstInterval; } }
    public float InputContinusouInterval { get { return mInputContinuousInterval; } }

    public bool IsAllButtonReleased { get { return mHoldButtons == nn.hid.NpadButton.None; } }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="input_first_interval">継続状態へ移るまでの最初の間隔(秒)</param>
    /// <param name="input_continuous_interval">継続入力の間隔(秒)</param>
    public InputAssistController(float input_first_interval = DEFAULT_FIRST_INPUT_INTERVAL, float input_continuous_interval = DEFAULT_CONTINUOUS_INPUT_INTERVAL)
    {
        mInputFirstInterval = input_first_interval;
        mInputContinuousInterval = input_continuous_interval;
    }

    #region AccessMethod
    /// <summary>
    /// 有効なボタンの取得
    /// </summary>
    /// <param name="buttons">ボタンのビットフラグ</param>
    /// <returns>有効なボタンのビットフラグ</returns>
    public nn.hid.NpadButton GetValidButtons(nn.hid.NpadButton buttons)
    {
        var now_time = Time.realtimeSinceStartup;

        int key;
        float button_time;
        bool is_continued;
        for (int i = 0; i < NPAD_BUTTONS.Length; i++)
        {
            if(buttons.Has(NPAD_BUTTONS[i]))
            {
                key = (int)NPAD_BUTTONS[i];

                if (mIsInputContinuedDic.TryGetValue(key, out is_continued) && mInputTimeDic.TryGetValue(key, out button_time))
                {
                    if (is_continued == false && now_time - button_time >= mInputFirstInterval)
                    {
                        mInputTimeDic[key] = now_time;
                        mIsInputContinuedDic[key] = true;
                    }
                    else if (is_continued == true && now_time - button_time >= mInputContinuousInterval)
                    {
                        mInputTimeDic[key] = now_time;
                    }
                    else
                    {
                        //連続入力時間の間隔が空いていなければ、対象のボタンを除外
                        buttons &= ~NPAD_BUTTONS[i];
                    }
                }
                else
                {
                    mInputTimeDic.Add(key, now_time);
                    mIsInputContinuedDic.Add(key, false);
                }
            }
        }

        return buttons;
    }

    /// <summary>
    /// ボタンの押下
    /// </summary>
    /// <param name="buttons">ボタンのビットフラグ</param>
    public void HoldButtons(nn.hid.NpadButton buttons)
    {
        mHoldButtons |= buttons;
    }

    /// <summary>
    /// ボタンの解放
    /// </summary>
    /// <param name="buttons">ボタンのビットフラグ</param>
    public void ReleaseButtons(nn.hid.NpadButton buttons)
    {
        mHoldButtons &= ~buttons;

        int key;
        for (int i = 0; i < NPAD_BUTTONS.Length; i++)
        {
            key = (int)NPAD_BUTTONS[i];

            if (buttons.Has(NPAD_BUTTONS[i]) && mInputTimeDic.ContainsKey(key))
            {
                mInputTimeDic.Remove(key);
            }
            if (buttons.Has(NPAD_BUTTONS[i]) && mIsInputContinuedDic.ContainsKey(key))
            {
                mIsInputContinuedDic.Remove(key);
            }
        }
    }
    #endregion
}
