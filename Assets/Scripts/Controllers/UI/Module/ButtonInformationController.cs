﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInformationController
{
    private Transform mRoot = null;

    private ButtonInfoUIPartController[] mButtonInformations = null;

    public ButtonInformationController(Transform root, int count)
    {
        mRoot = root;
        CreateButtonInformations(count);
    }

    #region AccessMethod
    /// <summary>
    /// ボタン情報パーツにパラメータを直接設定して表示する
    /// MAX_BUTTON_INFORMATIONS を超える数のボタン情報は表示できない
    /// ※info_textsとbuttonsは必ず一対一かつ同数になるようにして下さい
    /// </summary>
    /// <param name="info_texts">説明文の配列</param>
    /// <param name="buttons">ボタンの種類の配列</param>
    public void SetButtonInformations(string[] info_texts, nn.hid.NpadButton[] buttons)
    {
        DeactivateButtonInformations();

        if (info_texts == null || buttons == null) return;

        int count = Mathf.Min(info_texts.Length, buttons.Length);
        count = Mathf.Min(count, mButtonInformations.Length);

        for (int i = 0; i < count; ++i)
        {
            SetButtonInformation(i, info_texts[i], buttons[i]);
        }
    }

    /// <summary>
    /// 指定したインデックスのボタン情報パーツを設定する
    /// </summary>
    /// <param name="index">インデックス</param>
    /// <param name="text">説明文</param>
    /// <param name="button">ボタンの種類</param>
    public void SetButtonInformation(int index, string text, nn.hid.NpadButton button)
    {
        if (mButtonInformations[index] != null)
        {
            mButtonInformations[index].Activate();
            mButtonInformations[index].SetInfo(text, button);
        }
    }

    /// <summary>
    /// ボタン情報パーツを全て非アクティブにする
    /// </summary>
    public void DeactivateButtonInformations()
    {
        for (int i = 0; i < mButtonInformations.Length; ++i)
        {
            mButtonInformations[i].Deactivate();
        }
    }
    #endregion

    #region SystemMethod
    /// <summary>
    /// ボタン情報パーツを指定した数だけ生成する
    /// </summary>
    /// <param name="count">生成する数</param>
    private void CreateButtonInformations(int count)
    {
        mButtonInformations = new ButtonInfoUIPartController[count];

        for (int i = 0; i < mButtonInformations.Length; ++i)
        {
            mButtonInformations[i] = UIFactory.Instance.Create(Define.UI.PartTypeEnum.ButtonInfo, mRoot) as ButtonInfoUIPartController;
            mButtonInformations[i].Initialize();
        }
    }
    #endregion
}
