﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveButtonController : MonoBehaviour
{
    [SerializeField]
    private Text mTextName;
    [SerializeField]
    private Text mTextGameTime;
    [SerializeField]
    private Text mTextTitleSaveTime;
    [SerializeField]
    private Text mTextSaveTime;
    [SerializeField]
    private Transform mCursorRoot;
    [SerializeField]
    private Text mTextTitlePlayTime;
    [SerializeField]
    private Text mTextPlayTime;
    [SerializeField]
    private Image mGameClearImage;

    #region AccessMethod

    public void Initialize()
    {
        if (mTextTitleSaveTime != null) mTextTitleSaveTime.text = MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0009);
        if (mTextTitlePlayTime != null) mTextTitlePlayTime.text = MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0010);
    }

    /// <summary>
    /// ボタン名の設定
    /// </summary>
    /// <param name="name">ボタン名</param>
    public void SetName(string name)
    {
        mTextName.text = name;
    }

    /// <summary>
    /// ゲーム内時間の設定
    /// </summary>
    /// <param name="name">ゲーム内時間</param>
    public void SetGameTime(string name)
    {
        mTextGameTime.text = name;
    }

    /// <summary>
    /// セーブ時間の設定
    /// </summary>
    /// <param name="name">セーブ時間</param>
    public void SetSaveTime(string name)
    {
        mTextSaveTime.text = name;
    }

    /// <summary>
    /// 総プレイ時間の設定
    /// </summary>
    /// <param name="name"></param>
    public void SetPlayTime(string name)
    {
        mTextPlayTime.text = name;
    }

    /// <summary>
    /// カーソルの設定
    /// </summary>
    /// <param name="cursor">カーソル</param>
    public void SetCursor(Transform cursor)
    {
        if (cursor == null) return;

        cursor.SetParent(mCursorRoot, false);
    }

    /// <summary>
    /// ゲームをクリア済みかどうか設定
    /// </summary>
    /// <param name="is_clear">クリア済みかどうか</param>
    public void SetIsGameClear(bool is_clear)
    {
        if (is_clear) mGameClearImage.Activate();
        else mGameClearImage.Deactivate();
    }
    #endregion
}
