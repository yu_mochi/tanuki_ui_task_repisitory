﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.Events;
using System;

public class InfiniteScrollController : UIBehaviour
{
	[SerializeField, Header("複製元のオブジェクトのオリジナル")]
	private RectTransform mNodeOriginal;

	[SerializeField, Header("スクロール速度")]
	private float mScrollSpeed;

	[SerializeField, Header("入力後のカーソルの次の入力が可能になるまでの間隔")]
	private int mAddCountEnableCursor;

	[SerializeField, Header("リスト端から反対へ戻る際の速度")]
	private float mReturnSpeed;

	[SerializeField, Header("スクロールの方向を縦/横から指定")]
	private Direction mScrollDirection;

	[SerializeField, Header("スクロールのループ方法を指定する")]
	private Loop mLoopType;
	public Loop LoopType { get { return mLoopType; } }

	private CursorUIPartController mCursorController;
	public CursorUIPartController CursorController { set { mCursorController = value; } }

	[SerializeField, Header("ScrollRectをアタッチしたオブジェクト")]
	private ScrollRect mScrollRectangle;

	private RectTransform mScrollRectTransform;

	private ReverseAngle mReverse = ReverseAngle.Non;

	private ScrollAngle mAngle = ScrollAngle.Non;
	public ScrollAngle Angle { set { mAngle = value; } }

	private bool mIsEnableCursor = true;
	public bool IsEnableCursor { get { return mIsEnableCursor; } }

	private int mCountEnableCursor = 0;
	public int CountEnableCursor { get { return mCountEnableCursor; } }

	private bool mIsInLoop = false;

	private int mCurrentItemNo = 0;

	private int mSizeItemFullArray = 0;

	private float mPreFramePosition = 0;

	private float mScaleItem = -1;

	//TODO DebugText
	[SerializeField, Header("デバッグテキスト")]
	private Text mDebugText;

	private int mInitCreateNodes;
	public int InitCreateNodes { get { return mInitCreateNodes; } }

	private LinkedList<RectTransform> mItemList = new LinkedList<RectTransform>();

	private OnItemPositionChange mOnUpdateItem = new OnItemPositionChange();
	/// <summary>
	/// イベントを登録するアクセサ
	/// </summary>
	/// <param name="action">アイテム情報を更新するメソッド</param>
	public void AddListenerAction(UnityAction<int, GameObject> action) { mOnUpdateItem.AddListener(action); }

	/// <summary>
	/// スクロールの縦横
	/// </summary>
	private enum Direction
	{
		Vertical,
		Horizontal,
	}

	/// <summary>
	/// スクロールの具体的な方向
	/// </summary>
	public enum ScrollAngle
	{
		Up,
		Down,
		Non,
	}

	/// <summary>
	/// スクロールのループ設定
	/// </summary>
	public enum Loop
	{
		Loop,
		LimitedAndJump,
		Limited,
	}

	/// <summary>
	/// ScrollRect端まで動いた際、反対側へ戻るかを管理する
	/// </summary>
	private enum ReverseAngle
	{
		TopToBottom,
		BottomToTop,
		Non = -1,
	}

	private RectTransform mRect;
	private RectTransform Rect
	{
		get {
			if (mRect == null)
				mRect = gameObject.GetComponent<RectTransform>();
			return mRect;
		}
	}

	#region AccessMethod

	/// <summary>
	/// 生成したリストウィンドウ毎に持つInterfaceメソッドで初期化を行う
	/// </summary>
	/// <param name="size_array">生成時に使用した配列のサイズ</param>
	public void Initialize(int size_array)
	{
		mNodeOriginal.gameObject.Deactivate();

		mInitCreateNodes = CalcCreateNodes();

		mSizeItemFullArray = size_array;

		for (int i = 0; i < mInitCreateNodes; i++)
		{
			var item = GameObject.Instantiate(mNodeOriginal) as RectTransform;

			item.gameObject.name = i.ToString();

			item.SetParent(transform, false);

			item.anchoredPosition = mScrollDirection == Direction.Vertical ? new Vector2(0, -ScaleItem * i) : new Vector2(ScaleItem * i, 0);
			mItemList.AddLast(item);

			item.gameObject.Activate();

			mOnUpdateItem.Invoke(i, item.gameObject);
		}
		NodesBundleSetup(size_array);

		mScrollRectTransform = mScrollRectangle.gameObject.transform as RectTransform;
	}

	/// <summary>
	/// ScrollRectの上方向スクロール
	/// </summary>
	public void ScrollUp()
	{
		if (mLoopType == Loop.Loop)
		{
			ScrollUpLoop();
			return;
		}
		ScrollUpLimited();
	}

	/// <summary>
	/// ScrollRectの下方向スクロール
	/// </summary>
	public void ScrollDown()
	{
		if (mLoopType == Loop.Loop)
		{
			ScrollDownLoop();
			return;
		}
		ScrollDownLimited();
	}

	/// <summary>
	/// 基準となるアンカーポイントをスクロール方向に応じて返す
	/// </summary>
	public float AnchoredPosition
	{
		get {
			switch (mScrollDirection)
			{
				case Direction.Vertical:
					return -Rect.anchoredPosition.y;
				case Direction.Horizontal:
					return Rect.anchoredPosition.x;
				default:
					Debug.LogWarning("ScrollDirection is null");
					return 0.0f;
			}
		}
	}

	/// <summary>
	/// ループスクロールの際、反対側のアイテムの初期化を行う
	/// </summary>
	public void LoopOppositeUp()
	{
		mIsInLoop = true;
		mScrollRectangle.verticalNormalizedPosition = 1.0f;

		RectTransform[] item_rects = ItemRects();
		for (int i = 0; i < mInitCreateNodes; ++i)
		{
			mOnUpdateItem.Invoke(i, item_rects[i].gameObject);
		}
		mCountEnableCursor = mAddCountEnableCursor;
	}

	/// <summary>
	/// ループスクロールの際、反対側のアイテムの初期化を行う
	/// </summary>
	public void LoopOppositeDown()
	{
		mIsInLoop = true;
		mScrollRectangle.verticalNormalizedPosition = 0.0f;

		RectTransform[] item_rects = ItemRects();
		int item_obj = mInitCreateNodes - 1;

		for (int i = 0; i < mInitCreateNodes; ++i)
		{
			int intel_index = mSizeItemFullArray - 1;
			intel_index -= i;
			int item_index = mInitCreateNodes - 1 - i;
			mOnUpdateItem.Invoke(intel_index, item_rects[item_index].gameObject);
		}
		mCountEnableCursor = mAddCountEnableCursor;
	}

	/// <summary>
	/// 毎フレームの処理が終わったのちにReverseの必要を判断する
	/// </summary>
	public void IsReverse(nn.hid.NpadButton buttons)
	{
		if (mLoopType != Loop.LimitedAndJump)
			return;
		switch (buttons)
		{
			case nn.hid.NpadButton.StickLUp:
				if (IsMaxScroll() &&
					IsZeroCursorPosition())
				{
					mReverse = ReverseAngle.TopToBottom;
				}
				break;
			case nn.hid.NpadButton.StickLDown:
				if (IsMaxCursorBottom() &&
					IsMinScroll() &&
					mCursorController.CurrentIndex >= mItemList.Count - 2)
				{
					mReverse = ReverseAngle.BottomToTop;
				}
				break;
			default:
				Debug.LogWarning("Input button is not defined");
				break;
		}
	}

	/// <summary>
	/// カーソルの上底がスクロールコンテンツの上底に達しているかを返す
	/// </summary>
	/// <returns>カーソルがコンテンツの天井にいるか</returns>
	public bool IsMinCursorTop()
	{
		RectTransform[] item_rects = ItemRects();

		float cursor_top = Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y);

		if (cursor_top <= 0.0f)
			return true;

		return false;
	}

	/// <summary>
	/// カーソルの底辺がスクロールコンテンツの下底に達しているかを返す
	/// </summary>
	/// <returns>カーソルがコンテンツの底にいるか</returns>
	public bool IsMaxCursorBottom()
	{
		RectTransform[] item_rects = ItemRects();

		float cursor_bottom = Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y) + ScaleItem;

		if (cursor_bottom >= ScaleItem * mSizeItemFullArray)
			return true;

		return false;
	}

	/// <summary>
	/// 上限までスクロールしたかを返す
	/// </summary>
	/// <returns>スクロール上限か</returns>
	public bool IsMaxScroll()
	{
		if (mScrollRectangle.verticalNormalizedPosition >= 1.0f)
			return true;
		return false;
	}

	/// <summary>
	/// カーソルの上底がコンテンツ内の天井に到達しているかを返す
	/// </summary>
	/// <returns>カーソルが天井に届いているか</returns>
	public bool IsZeroCursorPosition()
	{
		RectTransform[] item_rects = ItemRects();

		float cursor_top = Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y);

		if (cursor_top <= 0)
			return true;
		return false;
	}

	#endregion

	#region SystemMethod

	/// <summary>
	/// スクロールはカーソルが画面外に出たことで行う
	/// </summary>
	/// <returns>カーソルは画面外か</returns>
	private bool IsOverScrollRectTop()
	{
		RectTransform[] item_rects = ItemRects();

		float cursor_top = Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y);

		float bar_position = (1.0f - mScrollRectangle.verticalNormalizedPosition) * Rect.sizeDelta.y;

		float view_area_top = bar_position - mScrollRectTransform.sizeDelta.y * (1.0f - mScrollRectangle.verticalNormalizedPosition);

		if (cursor_top < view_area_top)
		{
			mIsEnableCursor = false;
			return true;
		}
		return false;
	}

	/// <summary>
	/// スクロールはカーソルが画面外に出たことで行う
	/// </summary>
	/// <returns>カーソルは画面外か</returns>
	private bool IsOverScrollRectBottom()
	{
		RectTransform[] item_rects = ItemRects();

		float cursor_bottom = Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y) + ScaleItem;

		float bar_position = (1.0f - mScrollRectangle.verticalNormalizedPosition) * Rect.sizeDelta.y;

		float view_area_bottom = bar_position + mScrollRectTransform.sizeDelta.y * mScrollRectangle.verticalNormalizedPosition;

		if (cursor_bottom > view_area_bottom)
		{
			mIsEnableCursor = false;
			return true;
		}
		return false;
	}

	/// <summary>
	/// カーソルの上底がスクロールビューの上底に達しているかを返す
	/// </summary>
	/// <returns>カーソルがコンテンツの天井にいるか</returns>
	private bool IsReachCursorTopViewRect()
	{
		RectTransform[] item_rects = ItemRects();

		float cursor_top = Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y);

		float bar_position = (1.0f - mScrollRectangle.verticalNormalizedPosition) * Rect.sizeDelta.y;
		float invert_bar_position = 1 - mScrollRectangle.verticalNormalizedPosition;
		if (invert_bar_position < 0)
			invert_bar_position = 0;
		float view_area_top = bar_position - mScrollRectTransform.sizeDelta.y * (invert_bar_position);

		if (cursor_top <= view_area_top)
			return true;
		return false;
	}

	/// <summary>
	/// 下限までスクロールしたかを返す
	/// </summary>
	/// <returns>スクロール下限か</returns>
	private bool IsMinScroll()
	{
		if (mScrollRectangle.verticalNormalizedPosition <= 0.0f)
			return true;
		return false;
	}

	/// <summary>
	/// スクロールノードをまとめるオブジェクトのセットアップ
	/// </summary>
	/// <param name="size_array">生成元配列のサイズ</param>
	private void NodesBundleSetup(int size_array)
	{
		gameObject.GetComponentInParent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

		var rect_transform = gameObject.GetComponent<RectTransform>();
		Vector2 delta = rect_transform.sizeDelta;

		delta.y = ScaleItem * size_array;
		rect_transform.sizeDelta = delta;
	}

	/// <summary>
	/// 基準となるノードのサイズをスクロール方向に応じて返す
	/// </summary>
	private float ScaleItem
	{
		get {
			if (mNodeOriginal != null && mScaleItem == -1)
				mScaleItem = mScrollDirection == Direction.Vertical ? mNodeOriginal.sizeDelta.y : mNodeOriginal.sizeDelta.x;
			return mScaleItem;
		}
	}

	/// <summary>
	/// リスト表示領域に応じたノードの生成数を返す
	/// </summary>
	/// <returns>初期生成し使いまわすノードオブジェクト数</returns>
	private int CalcCreateNodes()
	{
		RectTransform rect = transform.parent.gameObject.GetComponent<RectTransform>();
		switch (mScrollDirection)
		{//ぴったりの数字だと表示に不都合があるため余分に生成しています
			case Direction.Vertical:
				return (int)(rect.sizeDelta.y / ScaleItem) + 2;
			case Direction.Horizontal:
				return (int)(rect.sizeDelta.x / ScaleItem) + 2;
			default:
				Debug.LogWarning("Scroll Direction is not assined");
				return -1;
		}
	}

	/// <summary>
	/// ScrollRectの反対側に到達するまで呼ばれ続ける
	/// </summary>
	private void ReverseMove()
	{
		switch (mReverse)
		{
			case ReverseAngle.TopToBottom:
				mCursorController.MoveDown();
				if (IsMinScroll())
					mReverse = ReverseAngle.Non;
				break;
			case ReverseAngle.BottomToTop:
				mCursorController.MoveUp();
				if (IsMaxScroll())
					mReverse = ReverseAngle.Non;
				break;
		}
	}

	/// <summary>
	/// ループ用上方向スクロール
	/// </summary>
	private void ScrollUpLoop()
	{
		if (IsOverScrollRectTop())
			Up(mScrollSpeed);
		else
		{
			mAngle = ScrollAngle.Non;
			mIsEnableCursor = true;
		}
		if (mScrollRectangle.verticalNormalizedPosition > 1.0f)
			mScrollRectangle.verticalNormalizedPosition = 1.0f;
	}

	/// <summary>
	/// ループ用下方向スクロール
	/// </summary>
	private void ScrollDownLoop()
	{
		if (IsOverScrollRectBottom())
			Down(mScrollSpeed);
		else
		{
			mAngle = ScrollAngle.Non;
			mIsEnableCursor = true;
		}
		if (mScrollRectangle.verticalNormalizedPosition < 0.0f)
			mScrollRectangle.verticalNormalizedPosition = 0.0f;
	}

	/// <summary>
	/// 制限あり上方向スクロール
	/// </summary>
	private void ScrollUpLimited()
	{
		if (IsOverScrollRectTop())
			Up(mScrollSpeed);
		else
		{
			mAngle = ScrollAngle.Non;
			mIsEnableCursor = true;
		}
		if (mScrollRectangle.verticalNormalizedPosition > 1.0f)
			mScrollRectangle.verticalNormalizedPosition = 1.0f;
	}

	/// <summary>
	/// 制限あり下方向スクロール
	/// </summary>
	private void ScrollDownLimited()
	{
		if (IsOverScrollRectBottom())
			Down(mScrollSpeed);
		else
		{
			mAngle = ScrollAngle.Non;
			mIsEnableCursor = true;
		}
		if (mScrollRectangle.verticalNormalizedPosition < 0.0f)
			mScrollRectangle.verticalNormalizedPosition = 0.0f;
	}

	/// <summary>
	/// 純粋な上方向スクロールのみを行う
	/// </summary>
	/// <param name="speed">目標のノードに到達するまでのスクロール速度</param>
	private void Up(float speed)
	{
		mScrollRectangle.verticalNormalizedPosition += speed;
	}

	/// <summary>
	/// 純粋な下方向スクロールのみを行う
	/// </summary>
	/// <param name="speed">目標のノードに到達するまでのスクロール速度</param>
	private void Down(float speed)
	{
		mScrollRectangle.verticalNormalizedPosition -= speed;
	}

	/// <summary>
	/// 画面端に到達しているかを返す
	/// </summary>
	/// <returns>カーソルは画面端か</returns>
	private bool CheckReverse()
	{
		if (mReverse != ReverseAngle.Non)
			return true;
		return false;
	}

	/// <summary>
	/// アイテム配列をインデックスで検索できる形で返す
	/// </summary>
	/// <returns>アイテム配列</returns>
	private RectTransform[] ItemRects()
	{
		RectTransform[] rects = new RectTransform[mItemList.Count];
		mItemList.CopyTo(rects, 0);
		return rects;
	}

	/// <summary>
	/// 各方向の座標判定の干渉を防止するためこのメソッドを通してスクロールする
	/// </summary>
	private void ScrollToScrollAngle()
	{
		switch (mAngle)
		{
			case ScrollAngle.Up:
				ScrollUp();
				break;
			case ScrollAngle.Down:
				ScrollDown();
				break;
			case ScrollAngle.Non:
				break;
		}
	}

	#endregion

	#region UnityMethod

	private void Update()
	{
		if (mCountEnableCursor > 0) mCountEnableCursor--;

		if (mItemList.First == null) return;

		if (CheckReverse()) ReverseMove();

		//下にスクロールしたときの処理
		while (AnchoredPosition - mPreFramePosition < -ScaleItem)
		{
			mPreFramePosition -= ScaleItem;

			var item = mItemList.First.Value;
			mItemList.RemoveFirst();
			mItemList.AddLast(item);

			if (mCursorController.CurrentIndex > 0 &&
				!mIsInLoop)
				mCursorController.DecrementIndex();

			float pos = ScaleItem * mInitCreateNodes + ScaleItem * mCurrentItemNo;
			item.anchoredPosition = (mScrollDirection == Direction.Vertical) ? new Vector2(0, -pos) : new Vector2(pos, 0);

			mOnUpdateItem.Invoke(mCurrentItemNo + mInitCreateNodes, item.gameObject);

			mCurrentItemNo++;
		}

		//上にスクロールしたときの処理
		while (AnchoredPosition - mPreFramePosition > 0)
		{
			mPreFramePosition += ScaleItem;

			var item = mItemList.Last.Value;
			mItemList.RemoveLast();
			mItemList.AddFirst(item);

			if (mCursorController.CurrentIndex < mItemList.Count - 1 &&
				!mIsInLoop)
				mCursorController.IncrementIndex();

			mCurrentItemNo--;

			float pos = ScaleItem * mCurrentItemNo;

			item.anchoredPosition = (mScrollDirection == Direction.Vertical) ? new Vector2(0, -pos) : new Vector2(pos, 0);

			mOnUpdateItem.Invoke(mCurrentItemNo, item.gameObject);
		}

		//親オブジェクトの変更
		if (mCursorController.IsChangeNodeNum)
		{
			RectTransform[] rect_array = ItemRects();

			GameObject parent = rect_array[mCursorController.CurrentIndex].gameObject;
			mCursorController.ChangeParent(parent);

			mCountEnableCursor = mAddCountEnableCursor;
		}

		//TODO Debug

		RectTransform[] item_rects = ItemRects();
		float cursor_button = (Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y) + ScaleItem);

		mDebugText.text =
			"CurrentIndex: " + mCursorController.CurrentIndex + "\n" +
			"ScaleItem * mSizeItemFullArray: " + (ScaleItem * mSizeItemFullArray) + "\n" +
			"verticalNormalizedPosition:" + mScrollRectangle.verticalNormalizedPosition + "\n" +
			"cursor_position: " + Math.Abs(item_rects[mCursorController.CurrentIndex].anchoredPosition.y) + "\n" +
			"cursor_bottom:" + cursor_button + "\n" +
			"mIsEnableCursor: " + mIsEnableCursor.ToString() + "\n" +
			"mCountEnableCursor: " + mCountEnableCursor;

		//TODO DebugEnd

		ScrollToScrollAngle();

		//ループ処理時の親変えとループの終了
		if (mIsInLoop == true)
		{
			mCursorController.ChangeParent(item_rects[mCursorController.CurrentIndex].gameObject);
			mIsInLoop = false;
		}
	}

	#endregion

	[System.Serializable]
	public class OnItemPositionChange : UnityEngine.Events.UnityEvent<int, GameObject> { }
}
