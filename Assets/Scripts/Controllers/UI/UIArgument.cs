﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UIManager.Instance.Open時に,各UIの初期化に必要なパラメータを渡す為の抽象クラス
/// パラメータを渡す必要がある場合はこのクラスを継承したクラスを,UIManager.Instance.Openに渡して下さい.
/// 各UIのInitializeにUIArgumentを渡すので,as演算子でキャストして使用して下さい.
/// </summary>
public abstract class UIArgument
{
}
