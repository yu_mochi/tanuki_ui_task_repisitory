﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUIPartAction
{
    /// <summary>
    /// 上移動の動作
    /// </summary>
    void MoveUpAction();

    /// <summary>
    /// 下移動の動作
    /// </summary>
    void MoveDownAction();

    /// <summary>
    /// 左移動の動作
    /// </summary>
    void MoveLeftAction();

    /// <summary>
    /// 右移動の動作
    /// </summary>
    void MoveRightAction();

    /// <summary>
    /// 決定の動作
    /// </summary>
    void SubmitAction();

    /// <summary>
    /// キャンセルの動作
    /// </summary>
    void CancelAction();
}
