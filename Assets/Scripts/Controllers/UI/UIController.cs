﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIController : MonoBehaviour
{
    private enum State
    {
        None,
        Open,
        Close,
    }

    protected static readonly nn.hid.NpadButton CURSOR_MOVE_UP_BUTTON = nn.hid.NpadButton.StickLUp | nn.hid.NpadButton.Up;
    protected static readonly nn.hid.NpadButton CURSOR_MOVE_DOWN_BUTTON = nn.hid.NpadButton.StickLDown | nn.hid.NpadButton.Down;
    protected static readonly nn.hid.NpadButton CURSOR_MOVE_LEFT_BUTTON = nn.hid.NpadButton.StickLLeft | nn.hid.NpadButton.Left;
    protected static readonly nn.hid.NpadButton CURSOR_MOVE_RIGHT_BUTTON = nn.hid.NpadButton.StickLRight | nn.hid.NpadButton.Right;
    protected static readonly nn.hid.NpadButton SUBMIT_BUTTON = nn.hid.NpadButton.A;
    protected static readonly nn.hid.NpadButton CANCEL_BUTTON = nn.hid.NpadButton.B;

    public Define.UI.TypeEnum Type { get; set; }
    public bool IsOpening { get { return mState == State.Open; } }
    public bool IsClosing { get { return mState == State.Close; } }
    public event System.Action DestroyedCallback;

    private State mState;
    private List<UIPartController> mUIParts = new List<UIPartController>();
    private System.Action mOpenedCallback;
    private System.Action<Define.UI.TypeEnum> mClosedCallback;

    private UIPartController mActionTargetUIPartController = null;
    private InputAssistController mInputAssist = new InputAssistController();
    private bool mUseInputAssist = true;
    private bool mIsEnableInput = true;

    /// <summary>
    /// 初期化
    /// ※UIManagerから呼び出される関数なので,その他の場所で呼ばないようにして下さい.
    /// </summary>
    /// <param name="arg">初期化用のパラメータ</param>
    public abstract void Initialize(UIArgument arg);

    /// <summary>
    /// 終了処理
    /// </summary>
    public virtual void Dispose() { }

    /// <summary>
    /// 破棄
    /// ※UIManagerから呼び出される関数なので,その他の場所で呼ばないようにして下さい.
    /// </summary>
    public void Destroy()
    {
        Dispose();
        if (DestroyedCallback != null) DestroyedCallback();
    }

    /// <summary>
    /// UIPartControllerを管理下におく
    /// </summary>
    /// <param name="part">自身が保持するUIPartController</param>
    public void AttachUIParts(UIPartController part)
    {
        if (part == null) return;
        mUIParts.Add(part);
    }

    /// <summary>
    /// 入力操作の対象となるUIPartControllerを登録
    /// </summary>
    /// <param name="target_part">対象となるUIPartController</param>
    /// <param name="use_input_assist">入力補助を使用するかどうか</param>
    protected void RegisterActionTargetUIPart(UIPartController target_part, bool use_input_assist = true)
    {
        mActionTargetUIPartController = target_part;

        mUseInputAssist = use_input_assist;

        if (mUseInputAssist && mInputAssist == null)
        {
            mInputAssist = new InputAssistController();
        }
    }

    /// <summary>
    /// UIを開く
    /// </summary>
    public void Open(System.Action opened_callback)
    {
        mOpenedCallback = opened_callback;

        mState = State.Open;
        OpeningCallback();
        OpeningUIPartsCallback();
        OpenUIParts();

        if (IsOpenedUIParts()) EndedOpenStateCallback();
    }

    /// <summary>
    /// UIを閉じる
    /// </summary>
    /// <param name="closed_callback">UIが閉じきった時に呼ばれるコールバック</param>
    public void Close(System.Action<Define.UI.TypeEnum> closed_callback)
    {
        mClosedCallback = closed_callback;

        mState = State.Close;
        ClosingCallback();
        ClosingUIPartsCallback();
        CloseUIParts();

        if (IsClosedUIParts()) EndedCloseStateCallback();
    }

    /// <summary>
    /// 入力の有効化
    /// </summary>
    public void EnableInput() { StartCoroutine(SetInputEnabled(true)); }

    /// <summary>
    /// 入力の無効化
    /// </summary>
    public void DisableInput() { StartCoroutine(SetInputEnabled(false)); }

    /// <summary>
    /// ボタンが押された時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">押されたボタンのビットフラグ</param>
    public virtual void InputButtonDownCallback(nn.hid.NpadButton buttons)
    {
        if (mActionTargetUIPartController == null || !mIsEnableInput) return;

        if (buttons.Has(SUBMIT_BUTTON))    //決定
        {
            mActionTargetUIPartController.SubmitAction();
        }
        else if (buttons.Has(CANCEL_BUTTON))    //キャンセル
        {
            mActionTargetUIPartController.CancelAction();
        }
    }

    /// <summary>
    /// ボタンが離された時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">離されたボタンのビットフラグ</param>
    public virtual void InputButtonUpCallback(nn.hid.NpadButton buttons)
    {
        //ボタン解放の記録
        mInputAssist.ReleaseButtons(buttons);
    }

    /// <summary>
    /// ボタンが押されている時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">押されているボタンのビットフラグ</param>
    public virtual void InputButtonCallback(nn.hid.NpadButton buttons)
    {
        //ボタン押下の記録
        mInputAssist.HoldButtons(buttons);

        if (mActionTargetUIPartController == null || !mIsEnableInput) return;

        //有効なボタンの取得
        if (mUseInputAssist) buttons = mInputAssist.GetValidButtons(buttons);

        //上移動の動作
        if (buttons.Has(CURSOR_MOVE_UP_BUTTON))
        {
            mActionTargetUIPartController.MoveUpAction();
        }
        //下移動の動作
        if (buttons.Has(CURSOR_MOVE_DOWN_BUTTON))
        {
            mActionTargetUIPartController.MoveDownAction();
        }
        //左移動の動作
        if (buttons.Has(CURSOR_MOVE_LEFT_BUTTON))
        {
            mActionTargetUIPartController.MoveLeftAction();
        }
        //右移動の動作
        if (buttons.Has(CURSOR_MOVE_RIGHT_BUTTON))
        {
            mActionTargetUIPartController.MoveRightAction();
        }
    }

    /// <summary>
    /// 左スティックに入力がある時に呼ばれるコールバック
    /// </summary>
    /// <param name="state">入力状態</param>
    public virtual void InputAnalogStickLCallback(nn.hid.AnalogStickState state) { }

    /// <summary>
    /// 左スティックが離された際に呼ばれるコールバック
    /// </summary>
    public virtual void InputAnalogStickLReleaseCallback() { }

    /// <summary>
    /// UIを開く時に呼ばれるコールバック
    /// </summary>
    protected virtual void OpeningCallback() { }

    /// <summary>
    /// UIが開ききった時に呼ばれるコールバック
    /// </summary>
    protected virtual void OpenedCallback() { }

    /// <summary>
    /// UIを閉じる時に呼ばれるコールバック
    /// </summary>
    protected virtual void ClosingCallback() { }

    /// <summary>
    /// UIが閉じ切った時に呼ばれるコールバック
    /// </summary>
    protected virtual void ClosedCallback() { }

#region UnityMethod
    private void Update()
    {
        switch (mState)
        {
            case State.Open:
                if (IsOpenedUIParts()) EndedOpenStateCallback();
                break;
            case State.Close:
                if (IsClosedUIParts()) EndedCloseStateCallback();
                break;
        }
    }
#endregion

#region SystemMethod
    /// <summary>
    /// 管理下の全UIPartの開くアニメーションを再生
    /// </summary>
    private void OpenUIParts()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy) mUIParts[i].Open();
        }
    }

    /// <summary>
    /// 管理下の全UIPartの'開く時に呼ばれるコールバック'を実行
    /// </summary>
    private void OpeningUIPartsCallback()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy) mUIParts[i].OpeningCallback();
        }
    }

    /// <summary>
    /// 管理下の全UIPartの'開ききった時に呼ばれるコールバック'を実行
    /// </summary>
    private void OpenedUIPartsCallback()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy) mUIParts[i].OpenedCallback();
        }
    }

    /// <summary>
    /// 管理下の全UIPartが開ききったかどうか
    /// </summary>
    /// <returns>[true:全て開ききった][false:全て開ききっていない]</returns>
    private bool IsOpenedUIParts()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy && !mUIParts[i].IsOpened()) return false;
        }
        return true;
    }

    /// <summary>
    /// Openステート終了時に呼ばれるコールバック
    /// </summary>
    private void EndedOpenStateCallback()
    {
        OpenedCallback();
        OpenedUIPartsCallback();
        mState = State.None;

        mOpenedCallback();
    }

    /// <summary>
    /// 管理下の全UIPartの閉じるアニメーションを再生
    /// </summary>
    private void CloseUIParts()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy) mUIParts[i].Close();
        }
    }

    /// <summary>
    /// 管理下の全UIPartの'閉じる時に呼ばれるコールバック'を実行
    /// </summary>
    private void ClosingUIPartsCallback()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy) mUIParts[i].ClosingCallback();
        }
    }

    /// <summary>
    /// 管理下の全UIPartの'閉じきった時に呼ばれるコールバック'を実行
    /// </summary>
    private void ClosedUIPartsCallback()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy) mUIParts[i].ClosedCallback();
        }
    }

    /// <summary>
    /// 管理下の全UIPartが閉じきったかどうか
    /// </summary>
    /// <returns>[true:全て閉じきった][false:全て閉じきっていない]</returns>
    private bool IsClosedUIParts()
    {
        for (int i = 0; i < mUIParts.Count; ++i)
        {
            if (mUIParts[i].gameObject.activeInHierarchy && !mUIParts[i].IsClosed()) return false;
        }
        return true;
    }

    /// <summary>
    /// Closeステート終了時に呼ばれるコールバック
    /// </summary>
    private void EndedCloseStateCallback()
    {
        ClosedCallback();
        ClosedUIPartsCallback();
        mState = State.None;

        mClosedCallback(Type);
    }

    /// <summary>
    /// 入力の有効/無効を設定
    /// ※全ボタンが解放されてから設定を行う
    /// </summary>
    /// <param name="is_enabled">有効/無効</param>
    /// <returns>IEnumerator</returns>
    private IEnumerator SetInputEnabled(bool is_enabled)
    {
        while (!mInputAssist.IsAllButtonReleased) yield return null;

        mIsEnableInput = is_enabled;
    }
#endregion
}
