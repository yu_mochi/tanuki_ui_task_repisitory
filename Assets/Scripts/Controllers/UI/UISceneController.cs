﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using nn.hid;

public class UISceneController : SceneController
{
	public override void FixedCreate()
	{
		InputManager.Instance.InputButtonDownCallback += InputButtonDownCallback;
	}

	private void InputButtonDownCallback(nn.hid.NpadButton buttons)
	{
		if (buttons.Has(nn.hid.NpadButton.Plus))
			UIManager.Instance.Open(Define.UI.TypeEnum.ShopWindow, new ShopWindowUIController.Argument(ShopModel()));
	}

	private void OnGUI()
	{
		GUIStyle box_style = new GUIStyle(GUI.skin.box);
		box_style.fontSize = 18;
		box_style.alignment = TextAnchor.UpperLeft;

		GUI.Box(new Rect(30, 30, 180, 120), "1キー:Aボタン\n2キー:Bボタン\n9キー:ロードUI\n0キー:セーブUI\n↑↓キー:カーソル移動", box_style);
	}

	/// <summary>
	/// 課題ではここで配列を宣言する
	/// 全部GetIDするのが健全な気がしたのですがDeifneに無かったのでとりま直接入れました
	/// </summary>
	/// <returns></returns>
	private ShopModel[] ShopModel()
	{
		ShopModel[] shop_models = new ShopModel[]
		{
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.A),NpadButton.A.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.B),NpadButton.B.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.X),NpadButton.X.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.Y),NpadButton.Y.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.L),NpadButton.L.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.R),NpadButton.R.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.ZL),NpadButton.ZL.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.ZR),NpadButton.ZR.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.LeftSL),NpadButton.LeftSL.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.RightSR),NpadButton.RightSR.ToString()),
			new ShopModel(201010110,"HOME"),
			new ShopModel(201010120,"CAPTURE"),
			new ShopModel(201010130,"Plus_J"),
			new ShopModel(201010140,"Minus_J"),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.Plus),NpadButton.Plus.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.Minus),NpadButton.Minus.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.StickL),NpadButton.StickL.ToString()),
			new ShopModel(Define.Sprite.Button.GetSpriteId(NpadButton.StickR),NpadButton.StickR.ToString()),
		};
		return shop_models;
	}
}
