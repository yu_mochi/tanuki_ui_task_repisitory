﻿/**
 *	@file	AbstractDialogUIController.cs
 *	@author	So Kadomoto
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダイアログを使用する UI Controller の基底となるクラス.
/// </summary>
public abstract class AbstractDialogUIController : UIController
{
    protected DialogUIPartController mDialog;

    // ☆なんかSEの設定とか色々あったけど必要ないから全部消しちゃった
}
