﻿using UnityEngine;
using nn.hid;

public class ShopWindowUIController : UIController
{
	public class Argument : UIArgument
	{
		private ShopModel[] mShopModel;
		public ShopModel[] ShopModel { get { return mShopModel; } }

		public Argument(ShopModel[] shop_model) { mShopModel = shop_model; }
	}

	private ShopWindowUIPartController mShopWindow;

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="arg">初期化に用いる引数クラス</param>
	public override void Initialize(UIArgument arg)
	{
		mShopWindow = UIFactory.Instance.Create(Define.UI.PartTypeEnum.ShopWindow, this.transform) as ShopWindowUIPartController;
		AttachUIParts(mShopWindow);
		RegisterActionTargetUIPart(mShopWindow);

		if (mShopWindow == null)
			return;

		Argument cast_arg = arg as Argument;
		if (cast_arg != null)
			mShopWindow.Initialize(cast_arg.ShopModel);
	}

	public override void InputButtonDownCallback(NpadButton buttons)
	{
		if (buttons.Has(CANCEL_BUTTON))
		{
			UIManager.Instance.Pop();
			return;
		}
	}

	public override void InputButtonCallback(NpadButton buttons)
	{
		if (buttons.Has(CURSOR_MOVE_UP_BUTTON))
		{
			mShopWindow.ScrollUp(buttons);
			return;
		}

		if (buttons.Has(CURSOR_MOVE_DOWN_BUTTON))
		{
			mShopWindow.ScrollDown(buttons);
			return;
		}
	}

	public override void InputButtonUpCallback(NpadButton buttons)
	{
		mShopWindow.LStickMomentOff();
	}
}