﻿using UnityEngine;
using nn.hid;
using UnityEngine.UI;

public class GadgetGetUIController : UIController
{
	public class Argument : UIArgument
	{
		private string mGadgetText;
		public string GadgetText{get{ return mGadgetText; }}

		private nn.hid.NpadButton mButton;
		public nn.hid.NpadButton Button{get{ return mButton; }}

		public Argument(string text, nn.hid.NpadButton button)
		{
			mGadgetText = text;
			mButton = button;
		}
	}

	private GadgetGetUIPartController mGadgetGet;

	public override void Initialize(UIArgument arg)
	{
		mGadgetGet = UIFactory.Instance.Create(Define.UI.PartTypeEnum.GadgetGet, this.transform) as GadgetGetUIPartController;
		AttachUIParts(mGadgetGet);
		RegisterActionTargetUIPart(mGadgetGet);

		if (mGadgetGet == null)
			return;

		Argument cast_arg = arg as Argument;
		if (cast_arg != null)
			mGadgetGet.Initialize(cast_arg.GadgetText,cast_arg.Button);
	}

	/// <summary>
	/// ボタンが押された時に呼ばれるコールバック
	/// </summary>
	/// <param name="buttons"></param>
	public override void InputButtonDownCallback(nn.hid.NpadButton buttons)
	{
		if (buttons.Has(CANCEL_BUTTON))
		{
			UIManager.Instance.Pop();
			return;
		}
	}
}