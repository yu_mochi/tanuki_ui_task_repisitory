﻿using System.Collections;
using System.Collections.Generic;
using nn.hid;
using UnityEngine;

public class TextDialogUIController : AbstractDialogUIController
{
    private static readonly NpadButton SPECIAL_BUTTON = NpadButton.X;

    public class Argument : UIArgument
    {
        public string Text { get; private set; }
        public string[] ButtonInfoTexts { get; private set; }
        public System.Action SelectedPositiveCallback { get; private set; }
        public System.Action SelectedNegativeCallback { get; private set; }
        public System.Action SelectedSpecialCallback { get; private set; }

        public Argument(string text, string[] button_info_texts, System.Action selected_positive_callback, System.Action selected_negative_callback, System.Action selected_special_callback = null)
        {
            Text = text;
            ButtonInfoTexts = button_info_texts;
            SelectedPositiveCallback = selected_positive_callback;
            SelectedNegativeCallback = selected_negative_callback;
            SelectedSpecialCallback = selected_special_callback;
        }
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="arg">初期化用のパラメータ</param>
    public override void Initialize(UIArgument arg)
    {
        mDialog = UIFactory.Instance.Create(Define.UI.PartTypeEnum.Dialog, this.transform) as DialogUIPartController;
        AttachUIParts(mDialog);
        RegisterActionTargetUIPart(mDialog);

        if (mDialog == null) return;
        mDialog.Initialize();

        Argument cast_arg = arg as Argument;
        if (cast_arg != null)
        {
            if (cast_arg.SelectedSpecialCallback == null)
            {
                mDialog.ShowFullText(cast_arg.Text, cast_arg.ButtonInfoTexts, cast_arg.SelectedPositiveCallback, cast_arg.SelectedNegativeCallback);
            }
            else
            {
                mDialog.ShowFullText(cast_arg.Text, cast_arg.ButtonInfoTexts, SPECIAL_BUTTON,
                    cast_arg.SelectedPositiveCallback, cast_arg.SelectedNegativeCallback, cast_arg.SelectedSpecialCallback);
            }
        }
    }

    /// <summary>
    /// ボタンが押された時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">押されたボタンのビットフラグ</param>
    public override void InputButtonDownCallback(NpadButton buttons)
    {
        base.InputButtonDownCallback(buttons);

        if (buttons.Has(SPECIAL_BUTTON))
        {
            mDialog.SpecialAction();
        }
    }
}
