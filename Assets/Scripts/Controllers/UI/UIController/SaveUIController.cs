﻿#define SAVE_EXECUTE_FOR_COROUTINE

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using nn.hid;

public class SaveUIController : UIController
{
    public class Argument : UIArgument
    {
        public TypeEnum Type { get; private set; }
        public bool IsGameCleared { get; private set; }
        public Argument(TypeEnum type, bool is_game_cleared = false)
        {
            Type = type;
            IsGameCleared = is_game_cleared;
        }
    }

    public enum TypeEnum
    {
        Save,
        Load,
    }

    private static readonly NpadButton[] BUTTUN_INFO_BUTTONS = new NpadButton[] { SUBMIT_BUTTON, CANCEL_BUTTON };

    private ISaveUIBehaviour mBehaviour;
    private SaveListUIPartController mSaveList;
    private ButtonInformationController mButtonInformationController;
    private TypeEnum mType;

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="arg">初期化用のパラメータ</param>
    public override void Initialize(UIArgument arg)
    {
        Argument cast_arg = arg as Argument;
        if (cast_arg != null)
        {
            mBehaviour = CreateBehaviour(cast_arg.Type, cast_arg.IsGameCleared);
        }
        else
        {
            // nullチェックするのは嫌なので,デフォルトでセーブタイプの挙動を返す
            mBehaviour = CreateBehaviour(TypeEnum.Save, false);
        }

        mType = cast_arg.Type;

        mSaveList = UIFactory.Instance.Create(Define.UI.PartTypeEnum.SaveList, this.transform) as SaveListUIPartController;
        AttachUIParts(mSaveList);
        RegisterActionTargetUIPart(mSaveList);

        if (mSaveList != null) mSaveList.Initialize(mBehaviour);

        var button_info_texts = new string[]
        {
            MasterManager.Instance.TextMaster.GetText(Define.Text.Common.ID_0000_0002),
            MasterManager.Instance.TextMaster.GetText(Define.Text.Common.ID_0000_0003)
        };

        Transform root = (mSaveList != null) ? mSaveList.GetButtonInfoRoot() : null;
        mButtonInformationController = new ButtonInformationController(root, BUTTUN_INFO_BUTTONS.Length);
        mButtonInformationController.SetButtonInformations(button_info_texts, BUTTUN_INFO_BUTTONS);
    }

    /// <summary>
    /// ボタンが押された時に呼ばれるコールバック
    /// </summary>
    /// <param name="buttons">押されたボタンのビットフラグ</param>
    public override void InputButtonDownCallback(nn.hid.NpadButton buttons)
    {
        if (mSaveList == null) return;

        if (buttons.Has(SUBMIT_BUTTON))
        {
            if (!mBehaviour.CanExecuteDecisionProcess(mSaveList.GetCurrentSaveSlot())) return;
            OpenDialog();
            return;
        }

        if (buttons.Has(CANCEL_BUTTON))
        {
            UIManager.Instance.Pop();
            return;
        }
    }

    /// <summary>
    /// UIタイプ毎に挙動を生成
    /// </summary>
    /// <param name="type">UIのタイプ</param>
    /// <param name="is_game_cleared">ゲームをクリア済みかどうか</param>
    /// <returns>挙動</returns>
    private ISaveUIBehaviour CreateBehaviour(TypeEnum type, bool is_game_cleared)
    {
        switch (type)
        {
            case TypeEnum.Save: return new SaveUIBehaviour(is_game_cleared);
            case TypeEnum.Load: return new LoadUIBehaviour();
        }
        return new SaveUIBehaviour(is_game_cleared);
    }

    /// <summary>
    /// ダイアログUIを開く
    /// </summary>
    private void OpenDialog()
    {
        var button_info_texts = new string[]
        {
            MasterManager.Instance.TextMaster.GetText(Define.Text.Common.ID_0000_0002),
            MasterManager.Instance.TextMaster.GetText(Define.Text.Common.ID_0000_0003)
        };

        TextDialogUIController.Argument arg = new TextDialogUIController.Argument(mBehaviour.GetConfirmationText(), button_info_texts,
            () =>
            {
                if (mSaveList == null) return;
                mBehaviour.ExecuteDecisionProcess(mSaveList.GetCurrentSaveSlot());
                mSaveList.UpdateSaveDataButtons();
                UIManager.Instance.Pop();
            },
            () =>
            {
                UIManager.Instance.Pop();
            });

        var dialog_controller = UIManager.Instance.Open(Define.UI.TypeEnum.TextDialog, arg) as TextDialogUIController;
    }
}
