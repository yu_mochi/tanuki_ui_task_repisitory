﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class UIPartController : MonoBehaviour, IUIPartAction
{
    private enum State
    {
        None,
        Opened,
        Closed,
    }

    private State mState;
    private CanvasGroup mCanvasGroup;
    private EventHandleableAnimator mOpeningAndClosingAnimator;
    private int? mOpenAnimationHash;
    private int? mCloseAnimationHash;

    // 各 SE の ON/OFF フラグ. スクリプト側での動的な指定 2018/07/03 Kadomoto.
    protected bool mIsOpenSeOff = false;
    public bool IsOpenSeOff { get { return mIsOpenSeOff; } set { mIsOpenSeOff = value; } }
    protected bool mIsCloseSeOff = false;
    public bool IsCloseSeOff { get { return mIsCloseSeOff; } set { mIsCloseSeOff = value; } }
    protected bool mIsSubmitSeOff = false;
    public bool IsSubmitSeOff { get { return mIsSubmitSeOff; } set { mIsSubmitSeOff = value; } }
    protected bool mIsCancelSeOff = false;
    public bool IsCancelSeOff { get { return mIsCancelSeOff; } set { mIsCancelSeOff = value; } }
    protected bool mIsCursorUpSeOff = false;
    public bool IsCursorUpSeOff { get { return mIsCursorUpSeOff; } set { mIsCursorUpSeOff = value; } }
    protected bool mIsCursorDownSeOff = false;
    public bool IsCursorDownSeOff { get { return mIsCursorDownSeOff; } set { mIsCursorDownSeOff = value; } }
    protected bool mIsCursorLeftSeOff = false;
    public bool IsCursorLeftSeOff { get { return mIsCursorLeftSeOff; } set { mIsCursorLeftSeOff = value; } }
    protected bool mIsCursorRightSeOff = false;
    public bool IsCursorRightSeOff { get { return mIsCursorRightSeOff; } set { mIsCursorRightSeOff = value; } }

    /// <summary>
    /// Contoroller以下を表示する
    /// </summary>
    public void Show() { if (mCanvasGroup != null) mCanvasGroup.alpha = 1.0f; }

    /// <summary>
    /// Contoroller以下を隠す
    /// </summary>
    public void Hide() { if (mCanvasGroup != null) mCanvasGroup.alpha = 0.0f; }

    /// <summary>
    /// 開くアニメーションのハッシュ値を設定
    /// </summary>
    /// <param name="hash">開くアニメーションのハッシュ値</param>
    public void SetOpenAnimationHash(int hash) { mOpenAnimationHash = hash; }

    /// <summary>
    /// 閉じるアニメーションのハッシュ値を設定
    /// </summary>
    /// <param name="hash">閉じるアニメーションのハッシュ値</param>
    public void SetCloseAnimationHash(int hash) { mCloseAnimationHash = hash; }

    /// <summary>
    /// 開ききったかどうか
    /// </summary>
    /// <returns>[true:開ききった][false:開ききってない]</returns>
    public bool IsOpened() { return (mState == State.Opened); }

    /// <summary>
    /// 閉じきったかどうか
    /// </summary>
    /// <returns>[true:閉じきった][false:閉じきっていない]</returns>
    public bool IsClosed() { return (mState == State.Closed); }

    /// <summary>
    /// UIを開く
    /// </summary>
    public void Open() { PlayOpenAnimation(); }

    /// <summary>
    /// UIを閉じる
    /// </summary>
    public void Close() { PlayCloseAnimation(); }

    /// <summary>
    /// UIを開く時に呼ばれるコールバック
    /// </summary>
    public virtual void OpeningCallback() { }

    /// <summary>
    /// UIが開ききった時に呼ばれるコールバック
    /// </summary>
    public virtual void OpenedCallback() { }

    /// <summary>
    /// UIを閉じる時に呼ばれるコールバック
    /// </summary>
    public virtual void ClosingCallback() { }

    /// <summary>
    /// UIが閉じ切った時に呼ばれるコールバック
    /// </summary>
    public virtual void ClosedCallback() { }

    /// <summary>
    /// 上移動の動作
    /// </summary>
    public virtual void MoveUpAction() { }

    /// <summary>
    /// 下移動の動作
    /// </summary>
    public virtual void MoveDownAction() { }

    /// <summary>
    /// 左移動の動作
    /// </summary>
    public virtual void MoveLeftAction() { }

    /// <summary>
    /// 右移動の動作
    /// </summary>
    public virtual void MoveRightAction() { }

    /// <summary>
    /// 決定の動作
    /// </summary>
    public virtual void SubmitAction() { }

    /// <summary>
    /// キャンセルの動作
    /// </summary>
    public virtual void CancelAction() { }

    /// <summary>
    /// 開くアニメーション再生
    /// </summary>
    private void PlayOpenAnimation()
    {
        if (mOpeningAndClosingAnimator == null || !mOpenAnimationHash.HasValue)
        {
            mState = State.Opened;
            return;
        }

        mState = State.None;
        mOpeningAndClosingAnimator.ClearEvent();
        mOpeningAndClosingAnimator.Play(mOpenAnimationHash.Value, () => mState = State.Opened);
    }

    /// <summary>
    /// 閉じるアニメーション再生
    /// </summary>
    private void PlayCloseAnimation()
    {
        if (mOpeningAndClosingAnimator == null || !mCloseAnimationHash.HasValue)
        {
            mState = State.Closed;
            return;
        }

        mState = State.None;
        mOpeningAndClosingAnimator.ClearEvent();
        mOpeningAndClosingAnimator.Play(mCloseAnimationHash.Value, () => mState = State.Closed);
    }

    private void Awake()
    {
        Transform root = this.transform.Find("Controller");
        if (root == null)
        {
            Debug.Assert(false, "Controller is not found !!!");
            return;
        }

        // Animatorが存在する場合にのみEventHandleableAnimatorをアタッチする
        if (root.GetComponent<Animator>() != null)
        {
            mOpeningAndClosingAnimator = root.gameObject.AddComponentWithoutOverlap<EventHandleableAnimator>();
        }

        mCanvasGroup = root.GetComponent<CanvasGroup>();
        Debug.Assert(mCanvasGroup != null, "CanvasGroup is not found !!!");
    }
}
