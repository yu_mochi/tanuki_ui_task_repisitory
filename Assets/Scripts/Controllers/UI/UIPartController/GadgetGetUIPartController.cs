﻿using UnityEngine;
using UnityEngine.UI;

public class GadgetGetUIPartController : UIPartController
{
	[SerializeField, Header("秘密道具を表示するImageオブジェクト")]
	private Image mImage;

	[SerializeField, Header("秘密道具の名前を表示するテキストオブジェクト")]
	private Text mText;

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="type">ボタンタイプ</param>
	/// <param name="text">テキスト</param>
	public void Initialize(string text, nn.hid.NpadButton button)
	{
		if (mText != null) mText.text = text;
		if (mImage != null) AtlasFactory.Instance.LoadSprite(Define.Sprite.COMMON_ATLAS_ID, Define.Sprite.Button.GetSpriteId(button), (sprite) => mImage.sprite = sprite);
	}
}