﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using nn.hid;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SaveListUIPartController : UIPartController
{
    [SerializeField]
    private Animator mAnimator;
    [SerializeField]
    private Text mTextTitle;
    [SerializeField]
    private Text mTextQuickTitle;
    [SerializeField]
    private Text mTextNormalTitle;
    [SerializeField]
    private SaveButtonController[] mSaveDataButtons;
    [SerializeField]
    private GameObject mQuickSaveRoot;
    [SerializeField]
    private Transform mButtonInfoRoot;

    private ISaveUIBehaviour mBehaviour;
    private CursorUIPartController mCursor;
    private int mCursorIndex;

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="behaviour">挙動</param>
    public void Initialize(ISaveUIBehaviour behaviour)
    {
        mBehaviour = behaviour;
        mCursor = UIFactory.Instance.Create(Define.UI.PartTypeEnum.Cursor, transform) as CursorUIPartController;
        mCursorIndex = 0;
        if (UserManager.Instance.SaveTitle != null)
        {
            mCursorIndex = Mathf.Max(mBehaviour.GetTopCursorIndex(), UserManager.Instance.SaveTitle.LatestSlot);
        }
        UpdateCursor();

        SetOpenAnimationHash(mBehaviour.GetOpenAnimationHash());

        if (mTextTitle != null) mTextTitle.text = mBehaviour.GetTitle();
        if (mTextQuickTitle != null) mTextQuickTitle.text = MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0000);
        if (mTextNormalTitle != null) mTextNormalTitle.text = MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0001);

        if (mBehaviour.ShouldActivateQuickSaveRoot())
        {
            mQuickSaveRoot.Activate();
        }
        else
        {
            mQuickSaveRoot.Deactivate();
        }
        UpdateSaveDataButtons();
    }

    /// <summary>
    /// ボタン情報のノード取得
    /// </summary>
    /// <returns>ボタン情報のノード</returns>
    public Transform GetButtonInfoRoot()
    {
        return mButtonInfoRoot;
    }

    /// <summary>
    /// 現在のセーブスロットを取得
    /// </summary>
    /// <returns>セーブスロット</returns>
    public Define.System.Save.SlotEnum GetCurrentSaveSlot()
    {
        return (Define.System.Save.SlotEnum)mCursorIndex;
    }

    /// <summary>
    /// 上移動の動作
    /// </summary>
    public override void MoveUpAction()
    {
        --mCursorIndex;
        if (mCursorIndex < mBehaviour.GetTopCursorIndex()) mCursorIndex = mSaveDataButtons.Length - 1;
        UpdateCursor();
    }

    /// <summary>
    /// 下移動の動作
    /// </summary>
    public override void MoveDownAction()
    {
        ++mCursorIndex;
        if (mCursorIndex >= mSaveDataButtons.Length) mCursorIndex = mBehaviour.GetTopCursorIndex();
        UpdateCursor();
    }

    /// <summary>
    /// セーブデータボタン更新
    /// </summary>
    public void UpdateSaveDataButtons()
    {
        for (int i = mBehaviour.GetTopCursorIndex(); i < mSaveDataButtons.Length; ++i)
        {
            if (mSaveDataButtons[i] == null) continue;
            mSaveDataButtons[i].Initialize();
            if (SaveDataManager.Instance.IsExist(Define.System.Save.SlotToFileName((Define.System.Save.SlotEnum)i)))
            {
                mSaveDataButtons[i].SetName(UserManager.Instance.SaveTitle.FarmNames[i]);
                mSaveDataButtons[i].SetGameTime(UserManager.Instance.SaveTitle.FarmTimes[i]);
                mSaveDataButtons[i].SetSaveTime(UserManager.Instance.SaveTitle.SaveTimes[i]);
                mSaveDataButtons[i].SetPlayTime(UserManager.Instance.SaveTitle.PlayTimes[i]);
                mSaveDataButtons[i].SetIsGameClear(UserManager.Instance.SaveTitle.IsGameCleareds[i]);
                continue;
            }
            mSaveDataButtons[i].SetName(MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0002));
            mSaveDataButtons[i].SetGameTime(string.Empty);
            mSaveDataButtons[i].SetSaveTime(string.Empty);
            mSaveDataButtons[i].SetPlayTime(string.Empty);
            mSaveDataButtons[i].SetIsGameClear(false);
        }
    }

    /// <summary>
    /// カーソル更新
    /// </summary>
    private void UpdateCursor()
    {
        if (mSaveDataButtons != null && mSaveDataButtons[mCursorIndex] != null)
        {
            if (mCursor != null)
            {
                mCursor.Activate();
                mSaveDataButtons[mCursorIndex].SetCursor(mCursor.transform);
                return;
            }
        }
        mCursor.Deactivate();
    }
}
