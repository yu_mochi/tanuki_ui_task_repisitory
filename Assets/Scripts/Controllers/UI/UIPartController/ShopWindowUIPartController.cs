﻿using UnityEngine;
using UnityEngine.UI;

class ShopWindowUIPartController : UIPartController
{
	[SerializeField, Header("スクロール処理を持つコントローラー")]
	private InfiniteScrollController mInfiniteScroll;

	private ShopModel[] mShopModels;

	private Transform mParentCursor = null;

	private CursorUIPartController mCursorController;

	#region AccessMethod

	/// <summary>
	/// スクロール機能に必要な初期化を行う
	/// </summary>
	/// <param name="shop_model">アイテム配列</param>
	public void Initialize(ShopModel[] shop_model)
	{
		mShopModels = shop_model;

		mInfiniteScroll.AddListenerAction(UpdateItem);
		mInfiniteScroll.Initialize(shop_model.Length);
	}

	/// <summary>
	/// アイテム配列を渡しノードの情報を更新する
	/// </summary>
	/// <param name="item_count">アイテム配列のインデックス</param>
	/// <param name="obj">アイテム情報の更新を行うGameObject</param>
	public void UpdateItem(int item_count, GameObject obj)
	{
		if (item_count < 0 ||
			item_count >= mShopModels.Length)
		{
			obj.Deactivate();
		}
		else
		{
			obj.Activate();

			var item = obj.GetComponent<ShopNodeUIPartController>();
			item.UpdateItem(mShopModels[item_count].Id, mShopModels[item_count].Name);

			if (mParentCursor == null)
			{
				mParentCursor = item.ParentCursor;
				mCursorController = UIFactory.Instance.Create(Define.UI.PartTypeEnum.Cursor, mParentCursor) as CursorUIPartController;
				mCursorController.Initialize(mInfiniteScroll, ParentTransform, mShopModels.Length);
			}
		}
	}

	/// <summary>
	/// InfiniteScrollControllerとの依存関係からスクロール処理を中継する
	/// </summary>
	public void ScrollUp(nn.hid.NpadButton buttons)
	{
		mCursorController.LStickHoldUp(buttons);
	}

	/// <summary>
	/// InfiniteScrollControllerとの依存関係からスクロール処理を中継する
	/// </summary>
	public void ScrollDown(nn.hid.NpadButton buttons)
	{
		mCursorController.LStickHoldDown(buttons);
	}

	/// <summary>
	/// InfiniteScrollControllerとの依存関係から入力処理を中継する
	/// </summary>
	public void LStickMomentOff()
	{
		mCursorController.LStickMomentOff();
	}

	#endregion

	#region SystemMethod

	/// <summary>
	/// ショップリスト用のカーソル親オブジェクトを返す
	/// </summary>
	/// <param name="obj"></param>
	/// <returns>ショップリスト用ノードオブジェクト</returns>
	private Transform ParentTransform(GameObject obj)
	{
		Transform parent;
		parent = obj.GetComponent<ShopNodeUIPartController>().ParentCursor;
		return parent;
	}

	#endregion

}