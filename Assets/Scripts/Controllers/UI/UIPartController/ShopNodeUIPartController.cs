﻿using UnityEngine;
using UnityEngine.UI;

public class ShopNodeUIPartController : MonoBehaviour
{
	[SerializeField, Header("アイテム名を表示するテキスト")]
	private Text mText;

	[SerializeField, Header("アイテムアイコンを表示するイメージ")]
	private Image mImage;
	
	[SerializeField, Header("Cursorの親オブジェクト")]
	private Transform mParentCursor;
	public Transform ParentCursor { get { return mParentCursor; } }

	#region AccessMethod

	/// <summary>
	/// アイテムの各コンポーネントを更新する
	/// </summary>
	/// <param name="id">スプライトID</param>
	/// <param name="name">アイテムの名前</param>
	public void UpdateItem(int id, string name)
	{
		if (mText != null)
			mText.text = name;
		if (mImage != null)
			AtlasFactory.Instance.LoadSprite(Define.Sprite.COMMON_ATLAS_ID, id, (sprite) => mImage.sprite = sprite);
	}

	#endregion
}