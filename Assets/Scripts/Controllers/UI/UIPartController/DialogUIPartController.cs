﻿using nn.hid;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class DialogUIPartController : UIPartController
{
    private static readonly int ENTER_ANIMATION_HASH = Animator.StringToHash("UI_Dialog_01_01");
    private static readonly int ENTER_CRAFT_ANIMATION_HASH = Animator.StringToHash("UI_Dialog_01_02");
    private static readonly int ENTER_COOKING_ANIMATION_HASH = Animator.StringToHash("UI_Dialog_01_03");
    private static readonly int ENTER_CRAFT_WALLFLOOR_ANIMATION_HASH = Animator.StringToHash("UI_Dialog_01_04");

    private static readonly NpadButton SUBMIT_BUTTON = NpadButton.A;
    private static readonly NpadButton CANCEL_BUTTON = NpadButton.B;

    private static readonly NpadButton[] BUTTUN_INFO_BUTTONS = new NpadButton[] { SUBMIT_BUTTON, CANCEL_BUTTON };

    private static readonly int MAX_INFO_BUTTON_NUM = 3;

    [SerializeField]
    private GameObject mFullTextObject = null;
    [SerializeField]
    private Text mFullText = null;
    [SerializeField]
    private Transform mButtonInfoRoot = null;

    private ButtonInformationController mButtonInformationController = null;

    private System.Action mOnPositiveResultCallback = null;
    private System.Action mOnNegativeResultCallback = null;
    private System.Action mOnSpecialResultCallback = null;

    private bool mIsSelectNum = false;
    private int mBasePrice = -1;
    private event System.Action OnUpdateNum = null;

    /// <summary>
    /// 初期化
    /// </summary>
    public void Initialize()
    {
        //コールバックの初期化
        mOnPositiveResultCallback = null;
        mOnNegativeResultCallback = null;
        mOnSpecialResultCallback = null;

        //ボタンインフォ
        mButtonInformationController = new ButtonInformationController(mButtonInfoRoot, MAX_INFO_BUTTON_NUM);
    }

    /// <summary>
    /// テキストのみ表示
    /// </summary>
    /// <param name="text">本文</param>
    /// <param name="button_info_texts">ボタン情報のテキスト配列</param>
    /// <param name="positive_callback">肯定的な結果のコールバック</param>
    /// <param name="negative_callback">否定的な結果のコールバック</param>
    public void ShowFullText(string text, string[] button_info_texts, System.Action positive_callback, System.Action negative_callback)
    {
        ShowFullText(text, button_info_texts, NpadButton.None, positive_callback, negative_callback, null);
    }

    /// <summary>
    /// テキストのみ表示
    /// </summary>
    /// <param name="text">本文</param>
    /// <param name="button_info_texts">ボタン情報のテキスト配列</param>
    /// <param name="positive_callback">肯定的な結果のコールバック</param>
    /// <param name="negative_callback">否定的な結果のコールバック</param>
    public void ShowFullText(string text, string[] button_info_texts, NpadButton special_button, System.Action positive_callback, System.Action negative_callback, System.Action special_callback)
    {
        //登場アニメーションを設定
        SetOpenAnimationHash(ENTER_ANIMATION_HASH);

        //コールバックの設定
        mOnPositiveResultCallback = positive_callback;
        mOnNegativeResultCallback = negative_callback;
        mOnSpecialResultCallback = special_callback;

        //オブジェクトの表示/非表示
        mFullTextObject.Activate();

        //本文
        mFullText.text = text;

        var button_list = new List<NpadButton>();
        if (positive_callback != null) button_list.Add(SUBMIT_BUTTON);
        if (special_callback != null) button_list.Add(special_button);
        if (negative_callback != null) button_list.Add(CANCEL_BUTTON);

        //ボタンインフォの設定
        mButtonInformationController.SetButtonInformations(button_info_texts, button_list.ToArray());
    }
    
    /// <summary>
    /// 決定の動作
    /// </summary>
    public override void SubmitAction()
    {
        if (mOnPositiveResultCallback != null) mOnPositiveResultCallback();
    }

    /// <summary>
    /// キャンセルの動作
    /// </summary>
    public override void CancelAction()
    {
        if (mOnNegativeResultCallback != null) mOnNegativeResultCallback();
    }

    /// <summary>
    /// 特殊ボタンの動作
    /// </summary>
    public void SpecialAction()
    {
        if (mOnSpecialResultCallback != null)
        {
            mOnSpecialResultCallback();
        }
    }

    /// <summary>
    /// テキストの設定
    /// </summary>
    /// <param name="text_ui">テキストUI</param>
    /// <param name="text">文章</param>
    private void SetText(Text text_ui, string text)
    {
        if (text == null) text_ui.Deactivate();
        else
        {
            text_ui.Activate();
            text_ui.text = text;
        }
    }

    /// <summary>
    /// ボタンインフォのテキスト取得
    /// </summary>
    /// <returns>テキストの配列</returns>
    private string[] GetButtonInfoSelectNumTexts()
    {
        return new string[]
        {
            MasterManager.Instance.TextMaster.GetText(Define.Text.Common.ID_0000_0002),
            MasterManager.Instance.TextMaster.GetText(Define.Text.Common.ID_0000_0003),
        };
    }
}
