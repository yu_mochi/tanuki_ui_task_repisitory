﻿using UnityEngine;
using System;

public class CursorUIPartController : UIPartController
{
	private static readonly int NORMAL_ANIMATION_HASH = Animator.StringToHash("UI_Cursor_01_01");
	private static readonly int FINGER_ONLY_ANIMATION_HASH = Animator.StringToHash("UI_Cursor_01_02");
	private static readonly int DOKODEMO_DOOR_ANIMATION_HASH = Animator.StringToHash("UI_Cursor_01_03");

	[SerializeField]
	private Animator mAnimator = null;

	private bool mIsChangeNodeNum = false;
	public bool IsChangeNodeNum { get { return mIsChangeNodeNum; } }

	private int mCurrentIndex = 0;
	public int CurrentIndex { get { return mCurrentIndex; } }

	private int mHash = 0;

	private int mCurrentDisplayArray;

	private bool mIsButtonDownMoment = true;

	private InfiniteScrollController mScrollController;

	private Func<GameObject, Transform> mChangeParentMethod;

	#region AccessMethod

	/// <summary>
	/// 通常状態へ移行
	/// </summary>
	public void ToNomal()
	{
		mAnimator.Play(NORMAL_ANIMATION_HASH);
		mHash = NORMAL_ANIMATION_HASH;
	}
	/// <summary>
	/// 指のみの状態へ移行.
	/// </summary>
	public void ToFingerOnly()
	{
		mAnimator.Play(FINGER_ONLY_ANIMATION_HASH);
		mHash = FINGER_ONLY_ANIMATION_HASH;
	}

	/// <summary>
	/// どこでもドア状態へ移行
	/// </summary>
	public void ToDokodemoDoor()
	{
		mAnimator.Play(DOKODEMO_DOOR_ANIMATION_HASH);
		mHash = DOKODEMO_DOOR_ANIMATION_HASH;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="scroll_controller">スクロール制御コントローラー</param>
	/// <param name="parent">親オブジェクト</param>
	/// <param name="max_items">アイテム配列サイズ</param>
	public void Initialize(InfiniteScrollController scroll_controller, Func<GameObject, Transform> parent, int max_items)
	{
		mChangeParentMethod = parent;
		mScrollController = scroll_controller;
		mCurrentDisplayArray = mScrollController.InitCreateNodes;
		mScrollController.CursorController = gameObject.GetComponent<CursorUIPartController>();
	}

	/// <summary>
	/// 配列のインデックスを加算する
	/// </summary>
	public void IncrementIndex()
	{
		if (mCurrentIndex < mCurrentDisplayArray - 1)
			mCurrentIndex++;
	}

	/// <summary>
	/// 配列のインデックスを減算する
	/// </summary>
	public void DecrementIndex()
	{
		mCurrentIndex--;
	}

	/// <summary>
	/// カーソル移動に必要な処理をまとめたメソッド
	/// </summary>
	public void MoveDown()
	{
		if (mScrollController.CountEnableCursor != 0)
			return;
		if (!mScrollController.IsMaxCursorBottom())
			IncrementIndex();
		mIsChangeNodeNum = true;
		mScrollController.Angle = InfiniteScrollController.ScrollAngle.Down;
	}

	/// <summary>
	/// 左スティック入力中呼ばれ続ける
	/// </summary>
	public void LStickHoldDown(nn.hid.NpadButton buttons)
	{
		if (mIsButtonDownMoment)
		{
			mScrollController.IsReverse(buttons);
			mIsButtonDownMoment = false;
		}
		if (!mScrollController.IsEnableCursor)
			return;
		if (mScrollController.LoopType == InfiniteScrollController.Loop.Loop &&
				mScrollController.IsMaxCursorBottom())
		{
			mScrollController.LoopOppositeUp();
			mCurrentIndex = 0;
			return;
		}
		MoveDown();
	}

	/// <summary>
	/// カーソル移動に必要な処理をまとめたメソッド
	/// </summary>
	public void MoveUp()
	{
		if (mScrollController.CountEnableCursor != 0)
			return;
		if (!IsMinCurrentIndex())
		{
			if (!mScrollController.IsZeroCursorPosition())
				DecrementIndex();
		}
		mIsChangeNodeNum = true;
		mScrollController.Angle = InfiniteScrollController.ScrollAngle.Up;
	}

	/// <summary>
	/// 左スティック入力中呼ばれ続ける
	/// </summary>
	public void LStickHoldUp(nn.hid.NpadButton buttons)
	{
		if (mIsButtonDownMoment)
		{
			mScrollController.IsReverse(buttons);
			mIsButtonDownMoment = false;
		}
		if (!mScrollController.IsEnableCursor)
			return;
		if (mScrollController.LoopType == InfiniteScrollController.Loop.Loop &&
				mScrollController.IsMinCursorTop())
		{
			mScrollController.LoopOppositeDown();
			mCurrentIndex = mCurrentDisplayArray - 2;
			return;
		}
		MoveUp();
	}

	/// <summary>
	/// 左スティックを離した瞬間だけ呼ばれる
	/// ボタン入力の瞬間の処理より先に入力中の処理が呼ばれてしまうため作りました
	/// </summary>
	public void LStickMomentOff()
	{
		mIsButtonDownMoment = true;
	}

	/// <summary>
	/// 親オブジェクトを変更し、変更を通知する
	/// </summary>
	/// <param name="obj">親オブジェクト</param>
	public void ChangeParent(GameObject obj)
	{
		Transform parent = mChangeParentMethod(obj);
		gameObject.transform.SetParent(parent, false);
		mIsChangeNodeNum = false;
	}

	/// <summary>
	/// カーソルの上底がスクロールビューの上底に達しているかを返す
	/// </summary>
	/// <returns>カーソルがスクロールビューの上底にいるか</returns>
	public bool IsMinCurrentIndex()
	{
		if (CurrentIndex <= 0)
			return true;
		return false;
	}

	#endregion

	#region SystemMethod

	private void Start()
	{
		// 生成と同じフレームで Animator のハッシュを指定しても Animator State が変更されないようなので, Start のタイミングで予約された変更を反映させます 2018/10/09 Kadomoto.
		if (mHash != 0)
		{
			mAnimator.Play(mHash);
		}
	}

	#endregion
}
