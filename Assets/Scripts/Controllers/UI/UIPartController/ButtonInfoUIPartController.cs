﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonInfoUIPartController : UIPartController
{
    [SerializeField]
    private UnityEngine.UI.Text mTextInfo;
    [SerializeField]
    private UnityEngine.UI.Image mIconButton;

    /// <summary>
    /// 初期化
    /// </summary>
    public void Initialize() { }

    /// <summary>
    /// ボタン情報設定
    /// </summary>
    /// <param name="info_text">説明文</param>
    /// <param name="button">ボタンの種類</param>
    public void SetInfo(string info_text, nn.hid.NpadButton button)
    {
        if (mTextInfo != null) mTextInfo.text = info_text;
        if (mIconButton != null) AtlasFactory.Instance.LoadSprite(Define.Sprite.COMMON_ATLAS_ID, Define.Sprite.Button.GetSpriteId(button), (sprite) => mIconButton.sprite = sprite);
    }
}