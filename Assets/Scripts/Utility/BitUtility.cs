﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BitUtility
{
    #region int
    public static bool Has(this int target, int value)
    {
        return (target & value) > 0;
    }

    public static int On(this int target, int value)
    {
        return (target | value);
    }

    public static int Off(this int target, int value)
    {
        return (target & ~value);
    }
    #endregion

    #region long
    public static bool Has(this long target, long value)
    {
        return (target & value) > 0;
    }

    public static long On(this long target, long value)
    {
        return (target | value);
    }

    public static long Off(this long target, long value)
    {
        return (target & ~value);
    }
    #endregion

    public static bool Has(this nn.hid.NpadButton target, nn.hid.NpadButton value)
    {
        return (target & value) > 0;
    }
}
