﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMultipleLoadHelper
{
    /// <summary>
    /// 積まれているリクエストの数
    /// </summary>
    int RequestCount { get; }

    /// <summary>
    /// 非同期ロードリクエスト追加
    /// </summary>
    /// <param name="path">リソースパス</param>
    /// <param name="extension">拡張子(.抜き)</param>
    /// <param name="language">言語</param>
    /// <param name="loaded_callback">ロード完了時に呼ばれるコールバック</param>
    void AddRequest(string path, string extension, string language, System.Action<Object> loaded_callback);

#if _DEBUG_BUILD
    /// <summary>
    /// ロード時間計測開始リクエスト
    /// </summary>
    void AddTimerStartRequest();

	/// <summary>
	/// ロード時間出力リクエスト
	/// </summary>
	/// <param name="text">出力時に頭に添える文字列</param>
	void AddLoadTimeOutputRequest(string text);

    /// <summary>
    /// リクエストをクリア
    /// </summary>
    void ClearRequest();
#endif

    /// <summary>
    /// Unloadリクエスト追加
    /// </summary>
    /// <param name="path">リソースパス</param>
    /// <param name="extension">拡張子(.抜き)</param>
    /// <param name="language">言語</param>
    void AddUnloadRequest(string path, string extension, string language);

    /// <summary>
    /// リクエストを順に全て実行する
    /// </summary>
    /// <param name="completed_callback">全てのリクエストが完了した際に呼ばれるコールバック</param>
    void ExecuteRequests(System.Action completed_callback);
}
