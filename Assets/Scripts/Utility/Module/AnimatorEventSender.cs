﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Animator のアニメーション開始通知と終了通知,更新通知を送信する Behaviour
/// Animator に Add Behaviour して利用します.
/// 尚,更新通知は最初と最後のフレームを除いて Update フレームごとに呼び出されます.
/// </summary>
public class AnimatorEventSender : StateMachineBehaviour
{
    private IAnimatorEventReceiver mReceiver;
    private bool mShouldGetReceiver = true;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo state_info, int layer_index)
    {
        ExecuteEvents.Execute<IAnimatorEventReceiver>(animator.gameObject, null, (receiver, event_data) => receiver.StateEnterCallback(state_info, layer_index));
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo state_info, int layer_index)
    {
        ExecuteEvents.Execute<IAnimatorEventReceiver>(animator.gameObject, null, (receiver, event_data) => receiver.StateExitCallback(state_info, layer_index));
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo state_info, int layer_index)
    {
        if (mReceiver == null) mReceiver = GetReceiver(animator);
        if (mReceiver == null) return;

        mReceiver.StateUpdateCallback(state_info, layer_index);
    }

    private IAnimatorEventReceiver GetReceiver(Animator animator)
    {
        if (mShouldGetReceiver)
        {
            mShouldGetReceiver = false;
            return animator.gameObject.GetComponent<IAnimatorEventReceiver>();
        }
        return null;
    }
}
