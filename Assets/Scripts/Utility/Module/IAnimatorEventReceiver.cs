﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Animator のイベントを受信する用のインターフェイス
/// </summary>
public interface IAnimatorEventReceiver : IEventSystemHandler
{
    /// <summary>
    /// ステート開始通知
    /// </summary>
    /// <param name="state_info">ステートの情報</param>
    /// <param name="layer_index">レイヤー番号</param>
    void StateEnterCallback(AnimatorStateInfo state_info, int layer_index);

    /// <summary>
    /// ステート終了通知
    /// </summary>
    /// <param name="state_info">ステートの情報</param>
    /// <param name="layer_index">レイヤー番号</param>
    void StateExitCallback(AnimatorStateInfo state_info, int layer_index);

    /// <summary>
    /// ステート更新通知
    /// </summary>
    /// <param name="state_info">ステートの情報</param>
    /// <param name="layer_index">レイヤー番号</param>
    void StateUpdateCallback(AnimatorStateInfo state_info, int layer_index);
}
