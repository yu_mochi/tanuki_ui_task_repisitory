﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UnityのAnimatorにイベント通知機能を持たせたラッパークラス
/// IAnimatorEventReceiver によって提供されているState〇〇Callbackは直接呼び出さないようにして下さい.
/// ※現在,Animatorのレイヤー番号を考慮した設計になっていません
/// </summary>
[RequireComponent(typeof(Animator))]
public class EventHandleableAnimator : MonoBehaviour, IAnimatorEventReceiver
{
    private Animator mAnimator;
    private int mStateNameHash;
    private int mLayerIndex;
    private Dictionary<int, System.Action> mEnteringCallbackTable = new Dictionary<int, System.Action>();
    private Dictionary<int, System.Action> mExitedCallbackTable = new Dictionary<int, System.Action>();

    #region AccessMethod
    /// <summary>
    /// 登録されたイベントを全て破棄
    /// </summary>
    public void ClearEvent()
    {
        mEnteringCallbackTable.Clear();
        mExitedCallbackTable.Clear();
    }

    /// <summary>
    /// アニメーション再生
    /// </summary>
    /// <param name="state_name_hash">再生するステートのハッシュ</param>
    /// <param name="exited_callback">再生終了時に呼ばれるコールバック</param>
    /// <param name="entering_callback">再生開始時に呼ばれるコールバック</param>
    public void Play(int state_name_hash, System.Action exited_callback = null, System.Action entering_callback = null)
    {
        Play(state_name_hash, 0, exited_callback, entering_callback);
    }

    /// <summary>
    /// アニメーション再生
    /// </summary>
    /// <param name="state_name_hash">再生するステートのハッシュ</param>
    /// <param name="layer_index">再生するレイヤー番号</param>
    /// <param name="exited_callback">再生終了時に呼ばれるコールバック</param>
    /// <param name="entering_callback">再生開始時に呼ばれるコールバック</param>
    public void Play(int state_name_hash, int layer_index, System.Action exited_callback, System.Action entering_callback)
    {
        if (mAnimator == null || !mAnimator.HasState(layer_index, state_name_hash))
        {
            if (entering_callback != null) entering_callback();
            if (exited_callback != null) exited_callback();
            return;
        }

        RegisterCallback(mEnteringCallbackTable, state_name_hash, entering_callback);
        RegisterCallback(mExitedCallbackTable, state_name_hash, exited_callback);

        if (mStateNameHash == state_name_hash && mLayerIndex == layer_index)
        {
            return;
        }

        mStateNameHash = state_name_hash;
        mLayerIndex = layer_index;
        mAnimator.Play(state_name_hash, layer_index);
    }

    /// <summary>
    /// AnimatorのON/OFF
    /// </summary>
    /// <param name="value"></param>
    public void SetEnabledAnimator(bool value)
    {
        if (mAnimator) mAnimator.enabled = value;
    }

    /// <summary>
    /// アニメーションのクロスフェード再生
    /// </summary>
    /// <param name="state_name_hash">再生するステートのハッシュ</param>
    /// <param name="transition_duration">遷移にかかる時間</param>
    /// <param name="layer_index">再生するレイヤー番号</param>
    /// <param name="exited_callback">再生終了時に呼ばれるコールバック</param>
    /// <param name="entering_callback">再生開始時に呼ばれるコールバック</param>
    public void CrossFadeInFixedTime(int state_name_hash, float transition_duration, int layer_index, System.Action exited_callback = null, System.Action entering_callback = null)
    {
        if (mAnimator == null || !mAnimator.HasState(layer_index, state_name_hash))
        {
            if (entering_callback != null) entering_callback();
            if (exited_callback != null) exited_callback();
            return;
        }

        RegisterCallback(mEnteringCallbackTable, state_name_hash, entering_callback);
        RegisterCallback(mExitedCallbackTable, state_name_hash, exited_callback);

        if (mStateNameHash == state_name_hash && mLayerIndex == layer_index)
        {
            return;
        }

        mStateNameHash = state_name_hash;
        mLayerIndex = layer_index;
        mAnimator.CrossFadeInFixedTime(state_name_hash, transition_duration, layer_index);
    }

    /// <summary>
    /// 特定のステートのアニメーションが再生中かどうか
    /// </summary>
    /// <param name="state_name_hash">ステートのハッシュ</param>
    /// <returns>[true:再生中][false:再生中ではない]</returns>
    public bool IsPlaying(int state_name_hash)
    {
        return (mAnimator.GetCurrentAnimatorStateInfo(0).shortNameHash == state_name_hash);
    }
    #endregion

    #region UnityMethod
    private void Awake()
    {
        mAnimator = GetComponent<Animator>();
    }
    #endregion

    #region SystemMethod
    /// <summary>
    /// ステート開始通知
    /// </summary>
    /// <param name="state_info">ステートの情報</param>
    /// <param name="layer_index">レイヤー番号</param>
    public void StateEnterCallback(AnimatorStateInfo state_info, int layer_index)
    {
        var callback = mEnteringCallbackTable.GetValue(state_info.shortNameHash);
        if (callback != null)
        {
            callback();
            mEnteringCallbackTable.Remove(state_info.shortNameHash);
        }
    }

    /// <summary>
    /// ステート終了通知
    /// </summary>
    /// <param name="state_info">ステートの情報</param>
    /// <param name="layer_index">レイヤー番号</param>
    public void StateExitCallback(AnimatorStateInfo state_info, int layer_index)
    {
        var callback = mExitedCallbackTable.GetValue(state_info.shortNameHash);
        if (callback != null)
        {
            callback();
            mExitedCallbackTable.Remove(state_info.shortNameHash);
        }
    }

    /// <summary>
    /// ステート更新通知
    /// </summary>
    /// <param name="state_info">ステートの情報</param>
    /// <param name="layer_index">レイヤー番号</param>
    public void StateUpdateCallback(AnimatorStateInfo state_info, int layer_index)
    {
        if (!state_info.loop && state_info.normalizedTime >= 1.0f)
        {
            var callback = mExitedCallbackTable.GetValue(state_info.shortNameHash);
            if (callback != null)
            {
                callback();
                mExitedCallbackTable.Remove(state_info.shortNameHash);
            }
        }
    }

    /// <summary>
    /// テーブルにコールバックを登録
    /// </summary>
    /// <param name="table">再生開始/終了時コールバックのテーブル</param>
    /// <param name="state_name_hash">再生するステートのハッシュ</param>
    /// <param name="callback">再生開始/終了時コールバック</param>
    private void RegisterCallback(Dictionary<int, System.Action> table, int state_name_hash, System.Action callback)
    {
        if (table == null || callback == null) return;
        if (table.ContainsKey(state_name_hash))
        {
            table[state_name_hash] += callback;
        }
        else
        {
            table.Add(state_name_hash, callback);
        }
    }
    #endregion
}
