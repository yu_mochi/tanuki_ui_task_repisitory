﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeCalculator
{
    private float mStartValue;
    private float mTargetValue;
    private float mFadeTime;
    private float mElapsedTime;

    /// <summary>
    /// パラメータ初期化
    /// </summary>
    /// <param name="start_value">遷移前の値</param>
    /// <param name="target_value">遷移後の値</param>
    /// <param name="fade_time">フェードに要する時間</param>
    public void Setup(float start_value, float target_value, float fade_time)
    {
        mStartValue = start_value;
        mTargetValue = target_value;
        mFadeTime = fade_time;
        mElapsedTime = 0.0f;
    }

    /// <summary>
    /// 線形補間計算
    /// </summary>
    /// <param name="delta_time">Time.deltaTime</param>
    /// <returns>線形補間された値</returns>
    public float CalculateValue(float delta_time)
    {
        mElapsedTime += delta_time;
        return (mFadeTime <= mElapsedTime) ? mTargetValue : Mathf.Lerp(mStartValue, mTargetValue, mElapsedTime / mFadeTime);
    }

    /// <summary>
    /// 遷移後の値だけ設定
    /// フェード中に遷移後の値を変えたい時に使用する
    /// </summary>
    /// <param name="target_value">遷移後の値</param>
    public void SetTargetValue(float target_value)
    {
        mTargetValue = target_value;
    }

    /// <summary>
    /// フェード終了チェック
    /// </summary>
    /// <returns>終了したかどうか</returns>
    public bool IsEnd() { return (mFadeTime <= mElapsedTime); }
}
