﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextUtility
{
    /// <summary>
    /// 季節をテキストIdに変換
    /// </summary>
    /// <param name="season">季節</param>
    /// <returns>テキストId</returns>
    public static int SeasonToTextId(Define.Time.SeasonEnum season)
    {
        switch (season)
        {
            case Define.Time.SeasonEnum.Spring: return Define.Text.Common.ID_0000_0005;
            case Define.Time.SeasonEnum.Summer: return Define.Text.Common.ID_0000_0006;
            case Define.Time.SeasonEnum.Autumn: return Define.Text.Common.ID_0000_0007;
            case Define.Time.SeasonEnum.Winter: return Define.Text.Common.ID_0000_0008;
            default:
                Debug.LogWarning(season.ToString() + " is not supported !!!");
                break;
        }
        return -1;
    }

    /// <summary>
    /// 季節をテキストに変換
    /// </summary>
    /// <param name="season">季節</param>
    /// <returns>季節名</returns>
    public static string SeasonToText(Define.Time.SeasonEnum season)
    {
        return MasterManager.Instance.TextMaster.GetText(SeasonToTextId(season));
    }

    /// <summary>
    /// 曜日をテキストIdに変換
    /// </summary>
    /// <param name="day_of_week">曜日</param>
    /// <returns>テキストId</returns>
    public static int DayOfWeekToTextId(Define.Time.DayOfWeek day_of_week)
    {
        switch (day_of_week)
        {
            case Define.Time.DayOfWeek.Sunday:      return Define.Text.Common.ID_0000_0009;
            case Define.Time.DayOfWeek.Monday:      return Define.Text.Common.ID_0000_0010;
            case Define.Time.DayOfWeek.Tuesday:     return Define.Text.Common.ID_0000_0011;
            case Define.Time.DayOfWeek.Wednesday:   return Define.Text.Common.ID_0000_0012;
            case Define.Time.DayOfWeek.Thursday:    return Define.Text.Common.ID_0000_0013;
            case Define.Time.DayOfWeek.Friday:      return Define.Text.Common.ID_0000_0014;
            case Define.Time.DayOfWeek.Saturday:    return Define.Text.Common.ID_0000_0015;
            default:
                Debug.LogWarning(day_of_week.ToString() + " is not supported !!!");
                break;
        }
        return -1;
    }

    /// <summary>
    /// 曜日をテキストに変換
    /// </summary>
    /// <param name="day_of_week">曜日</param>
    /// <returns>曜日名</returns>
    public static string DayOfWeekToText(Define.Time.DayOfWeek day_of_week)
    {
        return MasterManager.Instance.TextMaster.GetText(DayOfWeekToTextId(day_of_week));
    }
}
