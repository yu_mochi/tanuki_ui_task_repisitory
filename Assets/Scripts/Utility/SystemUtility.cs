﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SystemUtility
{
    /// <summary>
    /// [GameObject拡張メソッド]
    /// nullチェック付きactive化関数
    /// </summary>
    /// <param name="obj">GameObjct</param>
    public static void Activate(this GameObject obj)
    {
        if (obj == null) return;
        if (!obj.activeSelf) obj.SetActive(true);
    }

    /// <summary>
    /// [GameObject拡張メソッド]
    /// nullチェック付き非active化関数
    /// </summary>
    /// <param name="obj">GameObjct</param>
    public static void Deactivate(this GameObject obj)
    {
        if (obj == null) return;
        if (obj.activeSelf) obj.SetActive(false);
    }

    /// <summary>
    /// [GameObject拡張メソッド]
    /// 指定したComponentがアタッチされていなければアタッチし,
    /// 既にアタッチされているならそれを返す
    /// </summary>
    /// <param name="obj">GameObjct</param>
    public static T AddComponentWithoutOverlap<T>(this GameObject obj) where T : MonoBehaviour
    {
        if (obj == null) return null;
        T component = obj.GetComponent<T>();
        if (component != null) return component;
        return obj.AddComponent<T>();
    }

    /// <summary>
    /// [MonoBehaviour拡張メソッド]
    /// nullチェック付きactive化関数
    /// </summary>
    /// <param name="obj">MonoBehaviour</param>
    public static void Activate(this MonoBehaviour obj)
    {
        if (obj == null) return;
        obj.gameObject.Activate();
    }

    /// <summary>
    /// [MonoBehaviour拡張メソッド]
    /// nullチェック付き非active化関数
    /// </summary>
    /// <param name="obj">MonoBehaviour</param>
    public static void Deactivate(this MonoBehaviour obj)
    {
        if (obj == null) return;
        obj.gameObject.Deactivate();
    }

    /// <summary>
    /// [Dictionary拡張メソッド]
    /// TryGetValueで値を取得し,見つからなければデフォルト値を返す
    /// </summary>
    /// <typeparam name="K">Key</typeparam>
    /// <typeparam name="V">Value</typeparam>
    /// <param name="dic">Dictionary</param>
    /// <param name="key">検索したいKey</param>
    /// <param name="default_value">見つからなかった場合に返すデフォルト値</param>
    /// <returns>Dictionaryから取得した値orデフォルト値</returns>
    public static V GetValue<K, V>(this IDictionary<K, V> dic, K key, V default_value = default(V))
    {
        V value;
        if (dic.TryGetValue(key, out value)) return value;
        return default_value;
    }

    /// <summary>
    /// [Dictionary拡張メソッド]
    /// Keyから配列を生成して返す
    /// </summary>
    /// <typeparam name="K">Key</typeparam>
    /// <typeparam name="V">Value</typeparam>
    /// <param name="dic">Dictionary</param>
    /// <returns>Dictionaryに登録してある全てのKeyが格納された配列</returns>
    public static K[] KeyToArray<K, V>(this IDictionary<K, V> dic)
    {
        if (dic == null) return null;
        List<K> list = new List<K>(dic.Keys);
        return list.ToArray();
    }

    /// <summary>
    /// [Dictionary拡張メソッド]
    /// Valueから配列を生成して返す
    /// </summary>
    /// <typeparam name="K">Key</typeparam>
    /// <typeparam name="V">Value</typeparam>
    /// <param name="dic">Dictionary</param>
    /// <returns>Dictionaryに登録してある全てのValueが格納された配列</returns>
    public static V[] ValueToArray<K, V>(this IDictionary<K, V> dic)
    {
        if (dic == null) return null;
        List<V> list = new List<V>(dic.Values);
        return list.ToArray();
    }

    /// <summary>
    /// レイヤー設定
    /// 孫も含めて再帰的に設定する
    /// </summary>
    /// <param name="obj">対象となるオブジェクトのTransform</param>
    /// <param name="layer">設定したいレイヤーの名前</param>
    public static void SetLayersRecursively(Transform trans, string layer_name)
    {
        if (string.IsNullOrEmpty(layer_name)) return;
        SetLayersRecursively(trans.gameObject, LayerMask.NameToLayer(layer_name));
    }

    /// <summary>
    /// レイヤー設定
    /// 孫も含めて再帰的に設定する
    /// </summary>
    /// <param name="obj">対象となるGameObject</param>
    /// <param name="layer">設定したいレイヤーの名前</param>
    public static void SetLayersRecursively(GameObject obj, string layer_name)
    {
        if (string.IsNullOrEmpty(layer_name)) return;
        SetLayersRecursively(obj, LayerMask.NameToLayer(layer_name));
    }

    /// <summary>
    /// レイヤー設定
    /// 孫も含めて再帰的に設定する
    /// </summary>
    /// <param name="obj">対象となるオブジェクトのTransform</param>
    /// <param name="layer">設定したいレイヤーの番号</param>
    public static void SetLayersRecursively(Transform trans, int layer)
    {
        SetLayersRecursively(trans.gameObject, layer);
    }

    /// <summary>
    /// レイヤー設定
    /// 孫も含めて再帰的に設定する
    /// </summary>
    /// <param name="obj">対象となるGameObject</param>
    /// <param name="layer">設定したいレイヤーの番号</param>
    public static void SetLayersRecursively(GameObject obj, int layer)
    {
        if (obj == null || layer < 0) return;
        Transform[] children = obj.GetComponentsInChildren<Transform>(true);
        if (children == null) return;
        for (int i = 0; i < children.Length; ++i)
        {
            children[i].gameObject.layer = layer;
        }
    }

    /// <summary>
    /// タグ設定
    /// 孫も含めて再帰的に設定する
    /// </summary>
    /// <param name="trans">対象となるオブジェクトのTransform</param>
    /// <param name="tag">設定したいタグの名前</param>
    public static void SetTagsRecursively(Transform trans, string tag)
    {
        SetTagsRecursively(trans.gameObject, tag);
    }

    /// <summary>
    /// タグ設定
    /// 孫も含めて再帰的に設定する
    /// </summary>
    /// <param name="obj">対象となるGameObject</param>
    /// <param name="tag">設定したいタグの名前</param>
    public static void SetTagsRecursively(GameObject obj, string tag)
    {
        if (obj == null || string.IsNullOrEmpty(tag)) return;
        Transform[] children = obj.GetComponentsInChildren<Transform>(true);
        if (children == null) return;
        for (int i = 0; i < children.Length; ++i)
        {
            children[i].gameObject.tag = tag;
        }
    }

    /// <summary>
    /// [配列拡張メソッド]
    /// 配列から, 各要素に対応する確率分布に基づいて, ランダムに要素を取得.
    /// </summary>
    /// <param name="array"> ランダムに要素を取得したい配列. </param>
    /// <param name="array_prob"> 各要素に対応する確率分布（↑ と同じ長さの配列を指定すること） </param>
    public static T GetByProbability<T>(this T[] array, int[] array_prob)
    {
        if (array.Length != array_prob.Length)
        {
            Debug.LogError("確率分布の要素数が不正です.");
            return array[0];
        }

        int threshold = 0;
        int rand = Random.Range(0, 100);
        for (int i = 0; i < array_prob.Length; ++i)
        {
            threshold += array_prob[i];
            if (rand < threshold)
            {
                return array[i];
            }
        }

        // 本来はココまで到達しないが, 確率指定の総計が100%未満であれば到達する.
        return array[0]; // とりあえず1番目のやつを返しておく.
    }

    /// <summary>
    /// [List 拡張メソッド]
    /// リストからランダムに要素を取得.
    /// </summary>
    public static T GetByRandom<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    /// <summary>
    /// 各オブジェクトの生成と制御スクリプトの取得を行うジェネリックメソッド
    /// </summary>
    /// <typeparam name="T">制御スクリプト</typeparam>
    /// <param name="source">複製元のGameObject</param>
    /// <param name="parent">親のtransform</param>
    /// <returns>制御スクリプト</returns>
    public static T CreateController<T>(GameObject source, Transform parent) where T : MonoBehaviour
    {
        if (source == null)
        {
            Debug.Assert(false, "source is null !!!");
            return null;
        }

        T controller = null;
        GameObject obj = GameObject.Instantiate(source);
        if (obj != null)
        {
            if (parent != null) obj.transform.SetParent(parent, false);
            controller = obj.GetComponent<T>();
        }
        return controller;
    }

    public static GameObject LoadGameObject(string path)
    {
        Object src = Resources.Load<Object>(path);

        if (src == null)
        {
            Debug.Assert(false, path + " is not found !!!");
            return null;
        }

        return src as GameObject;
    }

    /// <summary>
    /// リソースパスを元にGameObjectを生成する
    /// </summary>
    /// <param name="path">リソースパス</param>
    /// <param name="parent">親のtransform</param>
    /// <returns>GameObject</returns>
    public static GameObject CreateGameObject(string path, Transform parent)
    {
        Object src = Resources.Load<Object>(path);

        if (src == null)
        {
            Debug.Assert(false, path + " is not found !!!");
            return null;
        }

        GameObject obj = Object.Instantiate(src, parent, false) as GameObject;
        if (obj == null)
        {
            Debug.Assert(false, path + " instantiate failed !!!");
            return null;
        }

        obj.name = src.name;
        return obj;
    }

    /// <summary>
    /// GameObjectをInstantiateする
    /// </summary>
    /// <param name="src">複製元のObject</param>
    /// <param name="parent">親のtransform</param>
    /// <returns>GameObject</returns>
    public static GameObject InstantiateGameObject(Object source, Transform parent)
    {
        if (source == null) return null;
        GameObject obj = Object.Instantiate(source, parent, false) as GameObject;
        obj.name = source.name;
        return obj;
    }

    /// <summary>
    /// オブジェクトのパスを取得
    /// </summary>
    /// <param name="tran">対象のTransform</param>
    /// <param name="root">ルートのTransform(指定するとルートまでのパスに限定する)</param>
    /// <returns>オブジェクトのパス</returns>
    public static string GetObjectPath(Transform tran, Transform root = null)
    {
        var path = tran.name;

        if (tran.parent != null && (root == null || tran.parent.name != root.name)) path = GetObjectPath(tran.parent, root) + "/" + path;

        return path;
    }

    /// <summary>
    /// インデックスを抽選して取得する
    /// </summary>
    /// <param name="rates">確率テーブル</param>
    /// <returns>抽選結果のインデックス</returns>
    public static int GetLotteryIndex(int[] rates)
    {
        if (rates == null) return -1;

        var total = 0;
        for (int i = 0; i < rates.Length; i++) total += rates[i];

        var rand = UnityEngine.Random.Range(1, total + 1);

        for (int i = 0; i < rates.Length; i++)
        {
            rand -= rates[i];
            if (rand <= 0) return i;
        }

        return rates.Length - 1;
    }

    /// <summary>
    /// お金の文字列表記を取得
    /// </summary>
    /// <param name="money">金額</param>
    /// <returns>金額の文字列</returns>
    public static string GetMoneyString(int money)
    {
        return money.ToString("N0");
    }

    /// <summary>
    /// 使用されていないリソースを解放
    /// </summary>
    /// <param name="callback">解放が完了した際に呼ばれるコールバック</param>
    /// <returns>IEnumerator</returns>
    public static IEnumerator UnloadUnusedAssets(System.Action callback)
    {
        AsyncOperation async = Resources.UnloadUnusedAssets();
        while (!async.isDone)
        {
            yield return 0;
        };
        if (callback != null) callback();
    }
}
