﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FarmModel
{
    [SerializeField]
    private string mName;
    public string Name { get { return mName; } }

    /// <summary>
    /// 牧場名の設定
    /// ※末尾に「牧場」を付けて格納する
    /// </summary>
    /// <param name="name">牧場名</param>
    public void SetName(string name)
    {
        mName = string.Format(MasterManager.Instance.TextMaster.GetText(Define.Text.Common.ID_0000_0045), name);
    }
}