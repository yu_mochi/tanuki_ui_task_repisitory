﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserModel
{
    [SerializeField]
    private TimeModel mTime = new TimeModel();
    public TimeModel Time { get { return mTime; } }

    [SerializeField]
    private FarmModel mFarm = new FarmModel();
    public FarmModel Farm { get { return mFarm; } }

    [SerializeField]
    private double mPlayTime = 0;
    public double PlayTime { get { return mPlayTime; } }

    private bool mIsGameCleared = false;
    public bool IsGameCleared { get { return mIsGameCleared; } set { mIsGameCleared = value; } }

    /// <summary>
    /// 総プレイ時間の追加
    /// MonoBehaviourを継承してるところで計測したい
    /// </summary>
    /// <param name="value">基本的にTime.deltaTimeの値</param>
    public void AddPlayTime(double value)
    {
        if (Define.Time.MAX_PLAY_TIME > mPlayTime + value) mPlayTime += value;
        else mPlayTime = Define.Time.MAX_PLAY_TIME;
    }
}
