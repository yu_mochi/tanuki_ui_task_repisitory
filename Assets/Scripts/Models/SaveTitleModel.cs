﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class SaveTitleModel
{
    [SerializeField]
    private string[] mFarmNames = new string[(int)Define.System.Save.SlotEnum.Length];
    public string[] FarmNames { get { return mFarmNames; } }

    [SerializeField]
    private string[] mFarmTimes = new string[(int)Define.System.Save.SlotEnum.Length];
    public string[] FarmTimes { get { return mFarmTimes; } }

    [SerializeField]
    private string[] mSaveTimes = new string[(int)Define.System.Save.SlotEnum.Length];
    public string[] SaveTimes { get { return mSaveTimes; } }

    [SerializeField]
    private string[] mPlayTimes = new string[(int)Define.System.Save.SlotEnum.Length];
    public string[] PlayTimes { get { return mPlayTimes; } }

    [SerializeField]
    private bool[] mIsGameCleareds = new bool[(int)Define.System.Save.SlotEnum.Length];
    public bool[] IsGameCleareds { get { return mIsGameCleareds; } }

    [SerializeField]
    private int mLatestSlot;
    public int LatestSlot { get { return mLatestSlot; } }

    // タイトル画面の季節分岐用 2018/09/12 Kadomoto.
    [SerializeField]
    private int mLatestDataSeason;
    public int LatestDataSeason { get { return mLatestDataSeason; } }

    [SerializeField]
    private bool mIsAfterGameCleared = false;
    public bool IsAfterGameCleared { get { return mIsAfterGameCleared; } }

    private System.Text.StringBuilder mStringBuilder = new System.Text.StringBuilder();

    [SerializeField]
    private bool mEndUserLicenseAgreement = false;
    public bool EndUserLicenseAgreement { get { return mEndUserLicenseAgreement; } }

    #region AccessMethod
    /// <summary>
    /// nullかどうか
    /// </summary>
    /// <returns>[true:null][false:nullではない]</returns>
    public bool IsNull() { return false; }

    /// <summary>
    /// null状態のModelを完全にnullにする
    /// </summary>
    public void CleanupNullModel() { }

    /// <summary>
    /// 特定スロットの情報を上書きする
    /// </summary>
    /// <param name="slot">上書きするスロット</param>
    /// <param name="farm_name">牧場名</param>
    /// <param name="farm_time">ゲーム内時間</param>
    /// <param name="save_time">セーブ時の現実時間</param>
    /// <param name="play_time">セーブ時の総プレイ時間(秒)</param>
    /// <param name="is_game_cleared">ゲームクリア済みフラグ</param>
    public void Overwrite(Define.System.Save.SlotEnum slot, string farm_name, TimeModel farm_time, DateTime save_time, double play_time, bool is_game_cleared)
    {
        UpdateLatestSlot(slot);
        mFarmNames[LatestSlot] = farm_name;

        mStringBuilder.Length = 0;
        mStringBuilder.AppendFormat(
            MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0005),
            farm_time.Year,
            TextUtility.SeasonToText(farm_time.OriginalSeason),
            farm_time.Day,
            farm_time.Hour,
            farm_time.Minute);
        mFarmTimes[LatestSlot] = mStringBuilder.ToString();

        mStringBuilder.Length = 0;
        mStringBuilder.AppendFormat(MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0006), save_time);
        mSaveTimes[LatestSlot] = mStringBuilder.ToString();

        int hour = 0, minutes = 0;
        mStringBuilder.Length = 0;
        ConvertPlayTime(play_time, out hour, out minutes);
        mStringBuilder.AppendFormat(MasterManager.Instance.TextMaster.GetText(Define.Text.UI.ID_1201_0007), hour, minutes);
        mPlayTimes[LatestSlot] = mStringBuilder.ToString();
        
        // ゲームクリア済みフラグ
        mIsGameCleareds[LatestSlot] = is_game_cleared;

        // タイトル画面の季節分岐用 2018/09/12 Kadomoto.
        mLatestDataSeason = (int)farm_time.OriginalSeason;
    }

    /// <summary>
    /// 直近の操作スロット更新
    /// </summary>
    /// <param name="slot">スロット</param>
    public void UpdateLatestSlot(Define.System.Save.SlotEnum slot)
    {
        mLatestSlot = (int)slot;
    }

    /// <summary>
    /// ゲームクリア直後フラグの設定
    /// </summary>
    /// <param name="is_game_cleared">ゲームクリア直後かどうか</param>
    public void SetIsAfterGameCleared(bool is_game_cleared)
    {
        mIsAfterGameCleared = is_game_cleared;
    }
    #endregion

    #region SystemMethod
    /// <summary>
    /// 総プレイ時間(秒)を時間と分に変換する
    /// </summary>
    /// <param name="time">総プレイ時間(秒)</param>
    /// <param name="hour">時間</param>
    /// <param name="minutes">分</param>
    private void ConvertPlayTime(double time, out int hour, out int minutes)
    {
        const int convert = 60;

        minutes = (int)time / convert;
        hour = minutes / convert;
        minutes -= hour * convert;
    }

    /// <summary>
    /// 利用規約同意
    /// </summary>
    /// <param name="is_agree"></param>
    public void SetEndUserLicenseAgreement(bool is_agree)
    {
        mEndUserLicenseAgreement = is_agree;
    }

    #endregion
}
