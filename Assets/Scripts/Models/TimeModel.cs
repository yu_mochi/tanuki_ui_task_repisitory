﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TimeModel
{
    [SerializeField]
    private int mYear = 1;
    public int Year { get { return mYear; } }

    [SerializeField]
    private Define.Time.SeasonEnum mSeason;
    public Define.Time.SeasonEnum Season
    {
        get
        {
            // 1日の深夜(0:00～5:59)は,前の季節として扱う
            if (mDay == 1 && mHour < Define.Time.MORNING)
            {
                var adjusted_season = mSeason - 1;
                if (adjusted_season == Define.Time.SeasonEnum.None) adjusted_season = Define.Time.SeasonEnum.Winter;
                return adjusted_season;
            }
            return mSeason;
        }
    }
    public Define.Time.SeasonEnum OriginalSeason { get { return mSeason; } }

    [SerializeField]
    private Define.Time.DayOfWeek mDayOfWeek = Define.Time.DayOfWeek.Monday;
    public Define.Time.DayOfWeek DayOfWeek { get { return mDayOfWeek; } }

    [SerializeField]
    private int mDay = 2;
    public int Day { get { return mDay; } }

    [SerializeField]
    private int mHour;
    public int Hour { get { return mHour; } }

    [SerializeField]
    private int mMinute;
    public int Minute { get { return mMinute; } }

    [SerializeField]
    private float mSecond;
    public float Second { get { return mSecond; } set { mSecond = value; } }

    public TimeModel()
    {
        mHour = Define.Time.MORNING;
    }

    public TimeModel(int hour, int minute) : this(0, Define.Time.SeasonEnum.Spring, 0, Define.Time.DayOfWeek.Length, hour, minute) { }
    public TimeModel(int year, Define.Time.SeasonEnum season, int day, Define.Time.DayOfWeek week = Define.Time.DayOfWeek.Length) : this(year, season, day, week, 0, 0) { }
    public TimeModel(int year, Define.Time.SeasonEnum season, int day, Define.Time.DayOfWeek week, int hour, int minute)
    {
        mYear = year;
        mSeason = season;
        mDay = day;
        mDayOfWeek = week;
        mHour = hour;
        mMinute = minute;
        CorrectYear();
    }

    public TimeModel(TimeModel src)
    {
        mYear = src.Year;
        mSeason = src.OriginalSeason;
        mDay = src.Day;
        mDayOfWeek = src.DayOfWeek;
        mHour = src.Hour;
        mMinute = src.mMinute;
        mSecond = src.Second;
        CorrectYear();
    }

    #region AccessMethod
    /// <summary>
    /// 指定した時間までスキップする
    /// ※指定した時間が現在時刻より過去だった場合はスキップに失敗する
    /// </summary>
    /// <param name="hour">時</param>
    /// <param name="minute">分</param>
    /// <returns>スキップに成功したかどうか</returns>
    public bool TrySkip(int hour, int minute)
    {
        if (hour < 0 || hour >= Define.Time.MAX_HOURS || minute < 0 || minute >= Define.Time.MAX_MINUTES) return false;
        if (hour == mHour && minute == mMinute) return false;

        int carry = 0;
        int diff_minute = minute - mMinute;
        if (diff_minute < 0)
        {
            ++carry;
            diff_minute += Define.Time.MAX_MINUTES;
        }

        int dest_hour = (hour < Define.Time.MORNING) ? hour + Define.Time.MAX_HOURS : hour;
        int current_hour = (mHour < Define.Time.MORNING) ? mHour + Define.Time.MAX_HOURS : mHour;
        int diff_hour = dest_hour - current_hour - carry;
        if (diff_hour < 0) return false;

        AdvanceMinutes(diff_minute);
        AdvanceHours(diff_hour);
        return true;
    }

    /// <summary>
    /// 次の日の朝までスキップする
    /// </summary>
    public void SkipToNextDay()
    {
        if (mHour < Define.Time.MORNING) AdvanceHours(Define.Time.MORNING - mHour);
        else AdvanceDays(1);
        mHour = Define.Time.MORNING;
        mMinute = 0;
    }

    /// <summary>
    /// 1分進める
    /// </summary>
    public void AdvanceMinute()
    {
        AdvanceMinutes(1);
    }

    /// <summary>
    /// 指定した分数分進める
    /// </summary>
    /// <param name="minutes">分数</param>
    public void AdvanceMinutes(int minutes)
    {
        mMinute += minutes;
        if (mMinute >= Define.Time.MAX_MINUTES)
        {
            int remainder;
            int hours = System.Math.DivRem(mMinute, Define.Time.MAX_MINUTES, out remainder);
            AdvanceHours(hours);
            mMinute = remainder;
        }
    }

    /// <summary>
    /// 指定した時間数分進める
    /// </summary>
    /// <param name="hours">時間数</param>
    public void AdvanceHours(int hours)
    {
        mHour += hours;
        if (mHour >= Define.Time.MAX_HOURS)
        {
            int remainder;
            int days = System.Math.DivRem(mHour, Define.Time.MAX_HOURS, out remainder);
            AdvanceDays(days);
            mHour = remainder;
        }
    }

    /// <summary>
    /// 指定した日数分進める
    /// </summary>
    /// <param name="days">日数</param>
    public void AdvanceDays(int days)
    {
        mDay += days;
        if (mDay >= Define.Time.MAX_DAYS)
        {
            int remainder;
            int seasons = System.Math.DivRem(mDay, Define.Time.MAX_DAYS, out remainder);
            AdvanceSeason(seasons);
            mDay = (remainder == 0) ? 1 : remainder;
        }

        mDayOfWeek += days;
        if (mDayOfWeek >= Define.Time.DayOfWeek.Length)
        {
            mDayOfWeek = (Define.Time.DayOfWeek)((int)mDayOfWeek % (int)Define.Time.DayOfWeek.Length);
        }
    }

    /// <summary>
    /// 日時を分に変換する
    /// </summary>
    /// <returns>時間(分)</returns>
    public int ConvertDateTimeToMinutes()
    {
        return ConvertDateToMinutes() + ConvertTimeToMinutes();
    }

    /// <summary>
    /// 日付を分に変換する
    /// </summary>
    /// <returns>時間(分)</returns>
    public int ConvertDateToMinutes()
    {
        int minutes = 0;

        minutes += mYear * Define.Time.CALC_YEAR_MINUTES;
        minutes += (int)mSeason * Define.Time.CALC_SEASON_MINUTES;
        minutes += mDay * Define.Time.CALC_DAY_MINUTES;

        return minutes;
    }

    /// <summary>
    /// 時刻を分に変換する
    /// </summary>
    /// <returns>時間(分)</returns>
    public int ConvertTimeToMinutes()
    {
        int minutes = 0;

        minutes += mHour * Define.Time.CALC_HOUR_MINUTES;
        minutes += mMinute;

        return minutes;
    }

    /// <summary>
    /// 日時の差分を取得する
    /// </summary>
    /// <param name="time">差分を取るTimeModel</param>
    /// <returns>時間(分)</returns>
    public int GetDiffDateTimeMinutes(TimeModel time)
    {
        return GetDiffDateMinutes(time) + GetDiffTimeMinutes(time);
    }

    /// <summary>
    /// 日付の差分を取得する
    /// </summary>
    /// <param name="time">差分を取るTimeModel</param>
    /// <returns>時間(分)</returns>
    public int GetDiffDateMinutes(TimeModel time)
    {
        int minutes = 0;

        minutes += (time.Year - mYear) * Define.Time.CALC_YEAR_MINUTES;
        minutes += (time.OriginalSeason - mSeason) * Define.Time.CALC_SEASON_MINUTES;
        minutes += (time.Day - mDay) * Define.Time.CALC_DAY_MINUTES;

        return minutes;
    }

    /// <summary>
    /// 時刻の差分を取得する
    /// </summary>
    /// <param name="time">差分を取るTimeModel</param>
    /// <returns>時間(分)</returns>
    public int GetDiffTimeMinutes(TimeModel time)
    {
        int minutes = 0;

        minutes += (time.Hour - Hour) * Define.Time.CALC_HOUR_MINUTES;
        minutes += time.Minute - Minute;

        return minutes;
    }

    /// <summary>
    /// 値がデフォルト値かどうか
    /// </summary>
    /// <returns>[true:デフォルト][false:デフォルトではない]</returns>
    public bool IsDefault()
    {
        if (mYear != 1) return false;
        if (mSeason != Define.Time.SeasonEnum.Spring) return false;
        if (mDayOfWeek != Define.Time.DayOfWeek.Monday) return false;
        if (mDay != 2) return false;
        if (mHour != Define.Time.MORNING) return false;
        if (mMinute != 0) return false;
        if (mSecond != 0) return false;
        return true;
    }
    #endregion

    #region SystemMethod
    /// <summary>
    /// 指定した季節数分進める
    /// </summary>
    /// <param name="seasons">季節数</param>
    private void AdvanceSeason(int seasons)
    {
        mSeason += seasons;
        if (mSeason >= Define.Time.SeasonEnum.Length)
        {
            int remainder;
            int years = System.Math.DivRem((int)mSeason, (int)Define.Time.SeasonEnum.Length, out remainder);
            AdvanceYears(years);
            mSeason = (Define.Time.SeasonEnum)remainder;
        }
    }

    /// <summary>
    /// 指定した年数分進める
    /// </summary>
    /// <param name="years">年数</param>
    private void AdvanceYears(int years)
    {
        mYear += years;
        CorrectYear();
    }

    /// <summary>
    /// 年度がカンスト値を超えないように補正する
    /// </summary>
    private void CorrectYear()
    {
        mYear = Mathf.Min(mYear, Define.Time.MAX_YEARS);
    }
    #endregion
}
