﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMasterModel
{
    int Id { get; }
}
