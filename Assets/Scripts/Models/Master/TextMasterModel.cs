﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMasterModel : IMasterModel
{
    private readonly int mId;
    public int Id { get { return mId; } }

    private readonly string mText;
    public string Text { get { return string.IsNullOrEmpty(mText) ? "" : mText; } }

    private readonly int mVoiceId = -1;
    public int VoiceId { get { return mVoiceId; } }

    public TextMasterModel(CCommonUIText.SCommonUIText text)
    {
        mId = text.mId;
        mText = text.mTextData;
    }

    public TextMasterModel(CUIText.SUIText text)
    {
        mId = text.mId;
        mText = text.mTextData;
    }
}
