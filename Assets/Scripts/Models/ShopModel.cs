﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ShopModel
{
	private int mId;
	public int Id { get { return mId; } }

	private string mName;
	public string Name { get { return mName; } }

	/// <summary>
	/// 
	/// </summary>
	/// <param name="id">商品ID</param>
	/// <param name="count">該当ショップに並べる商品数</param>
	/// <param name="name">商品名</param>
	public ShopModel(int id, string name)
	{
		mId = id;
		mName = name;
	}
}