﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFactory : SingletonMonoBehaviour<UIFactory>, IDontDestroy
{
    private bool mIsLoadedInAdvance;

    /// <summary>
    /// 受け取ったタイプ毎にUIを生成
    /// </summary>
    /// <param name="type">UIのタイプ</param>
    /// <param name="parent">親のtransform</param>
    /// <returns>制御スクリプト</returns>
    public UIController Create(Define.UI.TypeEnum type, Transform parent)
    {
        switch (type)
        {
            case Define.UI.TypeEnum.Save:
                return CreateUIController<SaveUIController>(type.ToString(), parent);
            case Define.UI.TypeEnum.TextDialog:
                return CreateUIController<TextDialogUIController>(type.ToString(), parent);
			case Define.UI.TypeEnum.GadgetGet:
				return CreateUIController<GadgetGetUIController>(type.ToString(), parent);
			case Define.UI.TypeEnum.ShopWindow:
				return CreateUIController<ShopWindowUIController>(type.ToString(), parent);
			default:
				Debug.Assert(false, type.ToString() + " is not found !!!");
                break;
        }
        return null;
    }

    /// <summary>
    /// 受け取ったタイプ毎にUIパーツを生成
    /// </summary>
    /// <param name="part_type">UIパーツのタイプ</param>
    /// <param name="parent">親のtransform</param>
    /// <returns>制御スクリプト</returns>
    public UIPartController Create(Define.UI.PartTypeEnum part_type, Transform parent)
    {
        string path = Define.UI.GetResourcePath(part_type);
        if (string.IsNullOrEmpty(path))
        {
            Debug.Assert(false, string.Format("[{0}] resource path is null !!!", part_type));
            return null;
        }

        GameObject obj = SystemUtility.CreateGameObject(path, parent);
        return (obj != null) ? obj.GetComponent<UIPartController>() : null;
    }

    /// <summary>
    /// GameObjectを生成し,制御スクリプトをアタッチして返す
    /// </summary>
    /// <typeparam name="T">UIControllerを継承した各UIの制御スクリプト</typeparam>
    /// <param name="name">GameObjectの名前</param>
    /// <param name="parent">親のtransform</param>
    /// <returns>制御スクリプト</returns>
    private UIController CreateUIController<T>(string name, Transform parent) where T : UIController
    {
        GameObject obj = new GameObject(name);
        if (parent != null)
        {
            obj.transform.SetParent(parent.transform, false);
            RectTransform obj_rect = obj.AddComponent<RectTransform>();
            obj_rect.anchorMin = Vector2.zero;
            obj_rect.anchorMax = Vector2.one;
            obj_rect.sizeDelta = Vector2.zero;
        }
        return obj.AddComponent<T>();
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
}
