﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class AtlasFactory : SingletonMonoBehaviour<AtlasFactory>, IDontDestroy
{
    private HashSet<int> mAtlases = new HashSet<int>();
    private Dictionary<int, Sprite> mSprites = new Dictionary<int, Sprite>();
    private System.Text.StringBuilder mStringBuilder = new System.Text.StringBuilder();

    /// <summary>
    /// Spriteをロード
    /// </summary>
    /// <param name="atlas_id">AtlasのId</param>
    /// <param name="sprite_id">SpriteのId</param>
    /// <param name="loaded_callback">ロード完了時に呼ばれるコールバック</param>
    public void LoadSprite(int atlas_id, int sprite_id, System.Action<Sprite> loaded_callback)
    {
        LoadSprites(atlas_id, sprite_id, loaded_callback);
    }

    /// <summary>
    /// Atlas単位でSpriteを一括ロード
    /// </summary>
    /// <param name="atlas_id">AtlasのId</param>
    /// <param name="sprite_id">SpriteのId</param>
    /// <param name="loaded_callback">ロード完了時に呼ばれるコールバック</param>
    private void LoadSprites(int atlas_id, int sprite_id, System.Action<Sprite> loaded_callback)
    {
        if (mAtlases.Contains(atlas_id))
        {
            if (loaded_callback != null) loaded_callback(mSprites.GetValue(sprite_id));
            return;
        }

        // ☆ほんとはアセットバンドルを非同期ロードしてたよ
        Object obj = Resources.Load(BuildString(Define.Sprite.ATLAS_RESOURCE_PATH, atlas_id));
        if (!TryCacheAtlas(atlas_id, obj))
        {
            if (loaded_callback != null) loaded_callback(null);
            return;
        }
        if (loaded_callback != null) loaded_callback(mSprites.GetValue(sprite_id));
    }

    /// <summary>
    /// Atlasのキャッシュを試みる
    /// </summary>
    /// <param name="atlas_id">AtlasのID</param>
    /// <param name="obj">ロードしたAtlasのObject</param>
    /// <returns>[true:成功][false:失敗]</returns>
    private bool TryCacheAtlas(int atlas_id, Object obj)
    {
        SpriteAtlas atlas = obj as SpriteAtlas;
        if (atlas == null)
        {
            Debug.LogWarning(BuildString("Failed to load atlas_{0} !!!", atlas_id));
            return false;
        }
        var sprites = new Sprite[atlas.spriteCount];
        atlas.GetSprites(sprites);

        mAtlases.Add(atlas_id);
        CacheSprites(sprites);
        return true;
    }

    /// <summary>
    /// Spriteをキャッシュする
    /// </summary>
    /// <param name="sprites">Spriteの配列</param>
    private void CacheSprites(Sprite[] sprites)
    {
        for (int i = 0; i < sprites.Length; ++i)
        {
            if (sprites[i] == null) continue;

            int id = 0;
            mStringBuilder.Length = 0;
            mStringBuilder.Append(sprites[i].name).Remove(0, sprites[i].name.IndexOf('_') + 1).Replace("(Clone)", "");
            
            if (!int.TryParse(mStringBuilder.ToString(), out id))
            {
                Debug.Assert(false, BuildString("int.Parse error !!! : {0}", sprites[i].name));
                continue;
            }

            if (mSprites.ContainsKey(id))
            {
                Debug.Assert(false, BuildString("This sprite has already been registered !!! : {0}", sprites[i].name));
                continue;
            }
            mSprites.Add(id, Instantiate(sprites[i]));
        }
    }

    /// <summary>
    /// 文字列を作成する
    /// </summary>
    /// <param name="format">書式指定文字列</param>
    /// <param name="args">書式項目</param>
    /// <returns>作成した文字列</returns>
    private string BuildString(string format, params object[] args)
    {
        mStringBuilder.Length = 0;
        mStringBuilder.AppendFormat(format, args);
        return mStringBuilder.ToString();
    }

    /// <summary>
    /// キャッシュをクリアする
    /// </summary>
    private void ClearCache()
    {
        mAtlases.Clear();
        mSprites.Clear();
    }

    public void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
    public void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to) { }
}
