﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T mInstance;
    public static T Instance
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = (T)FindObjectOfType(typeof(T));
                if (mInstance == null)
                {
                    Debug.LogWarning(typeof(T) + " is nothing");
                }
            }
            return mInstance;
        }
    }

    protected virtual void Awake()
    {
        CheckInstance();
    }

    protected virtual void OnDestroy()
    {
        mInstance = null;
    }

    private void CheckInstance()
    {
        if (this == Instance) return;
        GameObject.Destroy(this.gameObject);
    }
}
