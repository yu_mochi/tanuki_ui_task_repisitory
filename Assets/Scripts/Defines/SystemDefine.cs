﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Define
{
    public static class System
    {
        public static class Save
        {
            public static readonly string TITLE_FILE_NAME = "SaveTitle";

            public enum SlotEnum
            {
                Quick = 0,
                Save1,
                Save2,
                Save3,

                Length,
                None = -1,
            }

            /// <summary>
            /// セーブスロットをファイル名に変換する
            /// </summary>
            /// <param name="slot">セーブスロット</param>
            /// <returns>セーブ用のファイル名</returns>
            public static string SlotToFileName(SlotEnum slot)
            {
                switch (slot)
                {
                    case SlotEnum.Quick: return "QuickSaveData";
                    case SlotEnum.Save1: return "SaveData1";
                    case SlotEnum.Save2: return "SaveData2";
                    case SlotEnum.Save3: return "SaveData3";
                }
                return string.Empty;
            }
        }
    }
}
