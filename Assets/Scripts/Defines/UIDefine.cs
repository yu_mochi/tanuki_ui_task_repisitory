﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Define
{
    public static class UI
    {
        public enum TypeEnum
        {
            Save,
            TextDialog,
			GadgetGet,
			ShopWindow,

			Length,
            None = -1
        }

        public enum PartTypeEnum
        {
            ButtonInfo,
            Dialog,
            Cursor,
            SaveList,
			GadgetGet,
			ShopWindow,
			ShopList,

			Length,
            None = -1
        }

        /// <summary>
        /// UIパーツのリソースパスを取得
        /// </summary>
        /// <param name="type">UIパーツのタイプ</param>
        /// <returns>リソースパス</returns>
        public static string GetResourcePath(PartTypeEnum type)
        {
            switch (type)
            {
                case PartTypeEnum.ButtonInfo: return "Prefabs/UI/Farm/UI_ButtonInfo";
                case PartTypeEnum.Dialog: return "Prefabs/UI/Farm/UI_Dialog";
                case PartTypeEnum.Cursor: return "Prefabs/UI/Farm/UI_Cursor";
                case PartTypeEnum.SaveList: return "Prefabs/UI/Farm/UI_SaveList";
				case PartTypeEnum.GadgetGet: return "Prefabs/UI/Farm/UI_GadgetGet";
				case PartTypeEnum.ShopWindow: return "Prefabs/UI/Farm/UI_ShopWindow";
				case PartTypeEnum.ShopList: return "Prefabs/UI/Farm/UI_ShopList";

				default:
                    break;
            }
            return null;
        }
    }
}