﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Define
{
    public static class Sprite
    {
        public static readonly string ATLAS_RESOURCE_PATH = "atlas/atlas_{0}";
        public static readonly int UI_ATLAS_ID = 10101;
        public static readonly int COMMON_ATLAS_ID = 10201;
        public static readonly int ANIMAL_ATLAS_ID = 10201;
        public static readonly int CHARACTER_ATLAS_ID = 10201;
        public static readonly int BUILDING_ATLAS_ID = 10201;
        public static readonly int GROUND_ATLAS_ID = 90301;
        public static readonly int TAB_ATLAS_ID = 10201;
        public static readonly int COOKING_TOOL_ATLAS_ID = 20901;
        public static readonly int COOKING_RECIPE_ATLAS_ID = 20921;

        public static readonly int[] ATLAS_IDS = new int[]
        {
            20101,
            20111,
            20201,
            20301,
            20331,
            20341,
            20401,
            20410,
            20501,
            20601,
            20701,
            20901,
            20921,

            30100,
            30101,
            30102,
            30103,
            30104,
            30105,
            30106,
            30107,
            30108,
            30109,
            30110,
            30111,
            30112,
            30113,
            30114,
            30115,
            30116,
            30117,
            30118,
            30119,
            30120,
            30121,
            30122,
            30123,
            30124,
            30125,
            30126,
            30127,
            30128,
            30129,
            30130,
            30131,
            30132,
            30133,
            30134,

            90101,
            90201,
            90301,
        };

        public static class Button
        {
            public static readonly int ID_A         = 201010010;
            public static readonly int ID_B         = 201010020;
            public static readonly int ID_X         = 201010030;
            public static readonly int ID_Y         = 201010040;
            public static readonly int ID_L         = 201010050;
            public static readonly int ID_R         = 201010060;
            public static readonly int ID_ZL        = 201010070;
            public static readonly int ID_ZR        = 201010080;
            public static readonly int ID_SL        = 201010090;
            public static readonly int ID_SR        = 201010100;
            public static readonly int ID_HOME      = 201010110;
            public static readonly int ID_CAPTURE   = 201010120;
            public static readonly int ID_PLUS_J    = 201010130;
            public static readonly int ID_MINUS_J   = 201010140;
            public static readonly int ID_PLUS      = 201010150;
            public static readonly int ID_MINUS     = 201010160;
            public static readonly int ID_STICK_L   = 201010170;
            public static readonly int ID_STICK_R   = 201010180;

            /// <summary>
            /// ボタンSpriteId取得
            /// </summary>
            /// <param name="button">ボタンの種類</param>
            /// <returns>SpriteId</returns>
            public static int GetSpriteId(nn.hid.NpadButton button)
            {
                switch (button)
                {
                    case nn.hid.NpadButton.A:       return ID_A;
                    case nn.hid.NpadButton.B:       return ID_B;
                    case nn.hid.NpadButton.X:       return ID_X;
                    case nn.hid.NpadButton.Y:       return ID_Y;
                    case nn.hid.NpadButton.L:       return ID_L;
                    case nn.hid.NpadButton.R:       return ID_R;
                    case nn.hid.NpadButton.ZL:      return ID_ZL;
                    case nn.hid.NpadButton.ZR:      return ID_ZR;
                    case nn.hid.NpadButton.LeftSL:
                    case nn.hid.NpadButton.RightSL: return ID_SL;
                    case nn.hid.NpadButton.LeftSR:
                    case nn.hid.NpadButton.RightSR: return ID_SR;
                    case nn.hid.NpadButton.Plus:    return ID_PLUS;
                    case nn.hid.NpadButton.Minus:   return ID_MINUS;
                    case nn.hid.NpadButton.StickL:  return ID_STICK_L;
                    case nn.hid.NpadButton.StickR:  return ID_STICK_R;
                }
                return 0;
            }
        }
    }
}
