﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Define
{
    public static class Time
    {
        public enum SeasonEnum
        {
            Spring = 0,
            Summer,
            Autumn,
            Winter,

            Length,
            None = -1
        }

        public enum DayOfWeek
        {
            Sunday = 0,
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,

            Length
        }

        public static readonly int MAX_DAYS = 31;
        public static readonly int MAX_MINUTES = 60;
        public static readonly int MAX_HOURS = 24;
        public static readonly int MAX_YEARS = 99;
        public static readonly int MORNING = 6;

        //分に変換する時の計算用
        public static readonly int CALC_HOUR_MINUTES = MAX_MINUTES;
        public static readonly int CALC_DAY_MINUTES = MAX_MINUTES * MAX_HOURS;
        public static readonly int CALC_SEASON_MINUTES = MAX_MINUTES * MAX_HOURS * (MAX_DAYS - 1);
        public static readonly int CALC_YEAR_MINUTES = MAX_MINUTES * MAX_HOURS * (MAX_DAYS - 1) * (int)SeasonEnum.Length;

        public static readonly int MAX_PLAY_TIME = 3599940;
    }
}
