﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDontDestroy
{
    void CreateAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to);
    void DisposeAtScene(Define.Scene.NameEnum from, Define.Scene.NameEnum to);
}
